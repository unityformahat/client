import React, { Component } from 'react';
import BreadCrumbs from '../../components/BreadCrumbs/BreadCrumbs.js';
import BuildsWidget from '../../components/BuildsWidget/BuildsWidget.js';

class ProjectInfoPage extends Component {
    
    constructor(props) {
        super(props)
        this.state = {

        }
    }
    render() {
        return (
            <div className="ProjectInfoPage">
                <BreadCrumbs></BreadCrumbs>
                <div className="mainPage">
                    <div className="widget">
                        <BuildsWidget></BuildsWidget>
                    </div>
                </div>

            </div>
        );
    }
}

export default ProjectInfoPage;
