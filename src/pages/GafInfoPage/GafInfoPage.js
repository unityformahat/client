import React, { Component } from 'react';
import './GafInfoPage.css';
import BreadCrumbs from '../../components/BreadCrumbs/BreadCrumbs.js';
import DevelopmentProcess from '../../components/DevelopmentProcess/DevelopmentProcess.js';
import GafManagmentPage from '../../pages/GafInfoPage/GafManagmentPage/GafManagmentPage';
import GafDevelopmentPage from '../../pages/GafInfoPage/GafDevelopmentPage/GafDevelopmentPage';
import GafQAPage from '../../pages/GafInfoPage/GafQAPage/GafQAPage';
import GafSummaryPage from '../../pages/GafInfoPage/GafSummaryPage/GafSummaryPage';
import ProjectsMenu from '../../components/ProjectMenu/ProjectsMenu.js';
import AddGafWizard from '../../components/AddGafWizard/AddGafWizard.js';
import GafMonitoringPage from '../../pages/GafInfoPage/GafMonitoringPage/GafMonitoringPage';
import GafAutomaticMetricsPage from '../../pages/GafInfoPage/GafAutomaticMetricsPage/GafAutomaticMetricsPage';
import Drawer from '@material-ui/core/Drawer';
import { connect } from 'react-redux';
import Fab from '@material-ui/core/Fab';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import { selectGaf, fetchGafInformationIfNeeded } from '../../actions';
import PageLoading from '../../components/PageLoading/PageLoading';

class GafInfoPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            open: false, addServiceOpen: false,
        }
        this.scrollFunc = this.scrollFunc.bind(this);
    }

    scrollFunc = (event, hamburgerIcon, breadCrumbs) => {
        var breadCrumbsoffSet = breadCrumbs.offsetTop;
        if (window.pageYOffset >= breadCrumbsoffSet - 30) {
            hamburgerIcon.classList.add("progressHamburgerAfterScroll");
        } else {
            hamburgerIcon.classList.remove("progressHamburgerAfterScroll");
        }
    }

    // should move to store?
    handleToggle = () => this.setState({ open: !this.state.open })

    async componentDidMount() {
        const { dispatch } = this.props;
        await dispatch(selectGaf(this.props.match.params.unit, this.props.match.params.flight));
        const { selectedGaf, selectedUnit } = this.props.selectGaf;
        await dispatch(fetchGafInformationIfNeeded(selectedUnit.unit_id, selectedGaf.gaf_id))
    }

    async componentDidUpdate() {
        const { dispatch } = this.props;
        if (parseInt(this.props.match.params.flight) !== this.props.selectGaf.selectedGaf.gaf_id) {
            await dispatch(selectGaf(this.props.match.params.unit, this.props.match.params.flight));
            const { selectedGaf, selectedUnit } = this.props.selectGaf; 
            await dispatch(fetchGafInformationIfNeeded(selectedUnit.unit_id, selectedGaf.gaf_id));
        }
    }


    backToTop() {
        document.documentElement.scrollTop = 0;
    }

    render() {
        const { isFetching, gafData } = this.props
        
        return (
            <div className="GafInfoPage infoPage">
                <div id="topPageSticky">
                    <div id="breadCrumbs">
                        <BreadCrumbs></BreadCrumbs>
                    </div>
                    <div id="progressBar" >
                        <div id="processDiv">
                            <DevelopmentProcess isLoading={isFetching}></DevelopmentProcess>
                        </div>
                    </div>
                </div>
                <div className="infoPageMainContent">

                    <div>
                        <Fab color="primary" aria-label="add" id="fab" onClick={(e) => this.backToTop()}>
                            <ArrowUpwardIcon />
                        </Fab>
                    </div>
                    {
                        isFetching !== false ?
                            <PageLoading page="gafInfoPage" />
                            :
                    <div id="mainContent">
                        <div id="summary" className="pageContent">
                            <GafSummaryPage sprint={gafData.sprint[0]} criticalBugs={gafData.critical_bugs} lastPlanBuildsInfo={gafData.last_build_for_plan}></GafSummaryPage>
                        </div>
                        <div id="management" className="pageContent">
                            <GafManagmentPage issuesStatus={gafData.stories_status_data} workReports={gafData.work_reports}></GafManagmentPage>
                        </div>
                        <div id="development" className="pageContent">
                            <GafDevelopmentPage buildsInfo={gafData.last_builds} pullRequestsInfo={gafData.opened_pull_requests} commitsInfo={gafData.commits}></GafDevelopmentPage>
                        </div>
                        <div id="qa" className="pageContent">
                            <GafQAPage bugsInfo={gafData.bug_count}></GafQAPage>
                        </div>
                        <div id="monitor" className="pageContent">
                            <GafMonitoringPage></GafMonitoringPage>
                        </div>
                        <div id="automatic" className="pageContent">
                            <GafAutomaticMetricsPage></GafAutomaticMetricsPage>
                        </div>
                    </div>

                   }
                </div>
                <Drawer
                    anchor="right"
                    width='auto'
                    open={this.state.open}
                    onClose={this.handleToggle}>
                    <ProjectsMenu closeCallback={this.handleToggle} />
                    <AddGafWizard />
                </Drawer>
            </div>

        );
    }
}
function getUnitIndex(unitsArray, unitID) {
    var i=0;
    while (i < Object.keys(unitsArray).length) {
        if (unitsArray[i].unit_id === unitID) {
            return i;
        }
        i+=1;
    }
}

function getGafIndex(gafsArray, gafID) {
    var i=0;
    while (i < Object.keys(gafsArray).length) {
        if (gafsArray[i].gaf_id === gafID) {
            return i;
        }
        i+=1;
    }
}
const mapStateToProps = (state) => {
    const { selectGaf } = state;
    var gafInformation = undefined;
    if (selectGaf)
    {
        const unitIndex = getUnitIndex(state.hierarchy.units, selectGaf.selectedUnit.unit_id);
        const gafIndex = getGafIndex(state.hierarchy.units[unitIndex].gafs, selectGaf.selectedGaf.gaf_id);
        gafInformation = state.hierarchy.units[unitIndex].gafs[gafIndex].gafInformation;
    }
    const {
        isFetching,
        lastUpdated,
        gafData
    } = gafInformation || {
        isFetching: true,
    }
    return {
        selectGaf,
        isFetching,
        gafData,
        lastUpdated,

    }
}
export default connect(mapStateToProps)(GafInfoPage);
