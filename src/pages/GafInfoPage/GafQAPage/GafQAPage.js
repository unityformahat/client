import React, { Component } from 'react';
import './GafQAPage.css';
import Grid from '@material-ui/core/Grid';
import { Paper } from '@material-ui/core';
import { Chart } from 'chart.js';
import TestsPieChart from '../../../components/TestsPieChart/TestsPieChart';

class GafQAPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            bugsInfo: {
                Lowest: 57,
                Medium: 35,
                Highest: 8
            },
            leftTests: [
                {
                    type: "Gui",
                    label: "GUI",
                    passed: 35,
                    failed: 10,
                },
                {
                    type: "Integration",
                    label: "אינטגרציה רב-מערכתית",
                    passed: 500,
                    failed: 6565,
                },
            ],
            rightTests: [
                {
                    type: "Api",
                    label: "API",
                    passed: 990,
                    failed: 33,
                },
                {
                    type: "hand",
                    label: "ידניות",
                    passed: 111,
                    failed: 60,
                }
            ],
            unitTests: {
                type: "unit",
                label: "בדיקות יחידה",
                passed: 21,
                failed: 8,
                codeCoveragePercent: 88,
            }
        }
    }

    fillCharts = (test) => {
        const CHART = document.getElementById(test.type);
        new Chart(CHART, {
            type: "doughnut",
            data: {
                labels: ['עברו', 'נכשלו'],
                datasets: [
                    {
                        label: 'Results',
                        backgroundColor: ['#9ACB62', '#C25149'],
                        hoverBackgroundColor: ['#84bf40', '#ac4139'],
                        data: [test.passed, test.failed],
                    }
                ]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: false,
                },
                tooltips: {
                    mode: 'label',
                    intersect: true,
                    position: 'cursor',
                    backgroundColor: "#eaeaea",
                    bodyFontColor: "#4d4d4d",
                    bodyFontFamily: "Calibri",
                    bodyFontSize: 18,
                },
                cutoutPercentage: 80,
            }
        })
        Chart.Tooltip.positioners.cursor = function (CHART, coordinates) {
            return coordinates;
        }
    }

    fillCodeCoverageChart = () => {
        const CHART = document.getElementById("codeCoverageChart");
        window.myChart = new Chart(CHART, {
            type: "doughnut",
            data: {
                labels: [
                    "כיסוי קוד",
                    "עברו",
                    "נכשלו"
                ],
                datasets: [
                    // Outer doughnut data
                    {
                        data: [this.state.unitTests.codeCoveragePercent, 100 - this.state.unitTests.codeCoveragePercent],
                        labels: ["כיסוי קוד"],
                        backgroundColor: ["#79a6d2", "#fff"],
                        hoverBackgroundColor: ["#538cc6", "#fff"],
                        hoverBorderColor: ["#538cc6", "#fff"],
                    },
                    // Inner doughnut data
                    {
                        data: [this.state.unitTests.passed, this.state.unitTests.failed],
                        labels: ["עברו", "נכשלו"],
                        backgroundColor: ['#9ACB62', '#C25149'],
                        hoverBackgroundColor: ['#84bf40', '#ac4139'],

                    }
                ],

            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: false,
                },
                legendCallback: function(chart) {
                    var legendHTML = [];
                    var item = chart.data.datasets[0];
                    
                    legendHTML.push('<div class="chart-legend-color-desc-container">')
                    legendHTML.push('<div class="chart-legend" style="background-color:' + item.backgroundColor[0] + '"></div>');
                    legendHTML.push('</div>')
                    legendHTML.push('<div class="chart-legend-label-text">' + chart.data.labels[0] + ': ' + item.data[0] + '%</div>');
                    
                    return legendHTML.join("");
                },
                tooltips: {
                    mode: 'nearest',
                    intersect: true,
                    position: 'cursor',
                    backgroundColor: "#eaeaea",
                    bodyFontColor: "#4d4d4d",
                    bodyFontFamily: "Calibri",
                    bodyFontSize: 18,
                    animation: {
                        animateRotate: true
                    },
                    filter: function (item, data) {
                        var label = data.datasets[item.datasetIndex].labels[item.index];
                        if (label) return item;
                    },
                    callbacks: {
                        label: function (tooltipItem, data) {
                            var label = data.datasets[tooltipItem.datasetIndex].labels[tooltipItem.index];
                            label += ': ';
                            label += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                            if (data.datasets[tooltipItem.datasetIndex].labels[tooltipItem.index] === "כיסוי קוד") {
                                label += '%';
                            }
                            return label
                        }
                    }
                },
                cutoutPercentage: 65,
            }
        });
        Chart.Tooltip.positioners.cursor = function (CHART, coordinates) {
            return coordinates;
        }
        document.getElementById('codeCoverageLegend').innerHTML = window.myChart.generateLegend();
    }

    fillBugsChart = (bugsInfo) => {
        const CHART = document.getElementById("bugsChart");
        var ctx=CHART.getContext("2d");
        window.bugsChart = new Chart(CHART, {
            type: 'bar',
            data: {
                labels: [""],
                datasets: [
                    {
                        label: 'קל',
                        data: [bugsInfo.Lowest],
                        backgroundColor: "#f7db3b"
                    },
                    {
                        label: 'בינוני',
                        data: [bugsInfo.Medium],
                        backgroundColor: "#fd8535"
                    },
                    {
                        label:  'קריטי',
                        data: [bugsInfo.Highest],
                        backgroundColor: "#C25149"
                    }
                ]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: true,
                    position:'left',
                    labels: {
                        fontSize: 25,
                    },
                    textDirection: "rtl",
                    reverse: true,
                    onClick: null
                },
                tooltips: {
                    displayColors: false,
                    mode: 'nearest',
                    intersect: true,
                    position: 'cursor',
                    backgroundColor: "#eaeaea",
                    bodyFontColor: "#4d4d4d",
                    bodyFontFamily: "Calibri",
                    bodyFontSize: 18,
                    callbacks: {
                        label: function (tooltipItem, data) {
                            var label = data.datasets[tooltipItem.datasetIndex].label;
                            label += ': ';
                            label += data.datasets[tooltipItem.datasetIndex].data;
                            label += ' באגים';
                            return label
                        }
                    }
                },
                scales: {
                    xAxes: [{
                        display: false,
                        stacked: true,
                    }],
                    yAxes: [{
                        display: false,
                        stacked: true,
                        ticks: {mirror: true}
                    }]
                },
                animation: {
                    onProgress: function() {
                        var chartInstance = this.chart;
                        ctx = chartInstance.ctx;
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        ctx.fillStyle = 'white';
                        this.data.datasets.forEach(function (dataset) {
                            for (var i=0;i<dataset.data.length;i++){
                                var model=dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                                ctx.fillText(dataset.data[i], model.x,model.base);

                            }
                        });
                    }
                }

            },
            
        }, 
        {
        });
    }
    updateBugsChart(bugsChart, bugsInfo) {
        bugsChart.data.datasets[0].data = [bugsInfo.Lowest];
        bugsChart.data.datasets[1].data = [bugsInfo.Medium];
        bugsChart.data.datasets[2].data = [bugsInfo.Highest];
        bugsChart.update();
    }

    componentDidMount() {
        this.state.leftTests.map((test) => {
            return this.fillCharts(test);
        })
        this.state.rightTests.map((test) => {
            return this.fillCharts(test);
        })
        this.fillCodeCoverageChart();
        this.fillBugsChart(this.props.bugsInfo);

    }
    componentDidUpdate() {
        this.state.leftTests.map((test) => {
            return this.fillCharts(test);
        })
        this.state.rightTests.map((test) => {
            return this.fillCharts(test);
        })
        this.fillCodeCoverageChart();
        this.updateBugsChart(window.bugsChart, this.props.bugsInfo);
    }

    render() {
        return (
            <div className="tabPage">
                {/* Whole Page Container */}
                <Grid item container xs={11} sm={11} md={11} lg={11} xl={11} direction="row" id="qaPageContainer" className="widgetsSection">
                    {/* Tests Container */}
                    <Grid item container xs={12} sm={12} md={12} lg={9} xl={9} id="testsGridContainer">
                        <Paper id="testsPaper">
                            {/* Left Tests Container */}
                            <Grid item container direction="column" id="leftTestsContainer">
                                {this.state.leftTests.map((test) => {
                                    return <TestsPieChart key={test.type} test={test}></TestsPieChart>
                                })}
                            </Grid>

                            {/* Unit Tests Container */}
                            <Grid item container direction="row" id="unitTestsContainer">
                                {/* Unit Tests */}
                                <Grid item xs={12} id="centerGrid">
                                    
                                    <div id="codeCoverageLegend">
                                    </div>
                                    <div className="canvasParentDiv" id="mainChart" style={{ position: "relative", width:"15vw"}}>
                                        <div style={{ width: "55%", height: "40%", position: "absolute", top: "35%", left: "22%", marginTop: "-20px", textAlign: "center", zIndex: "3" }}>

                                            <div style={{ lineHeight: "40px" }}>
                                                <h1 style={{ fontSize: "2rem", fontWeight: "normal", marginBlockStart: "0", marginBlockEnd: "0" }}> {this.state.unitTests.label} </h1>
                                            </div>

                                            <div style={{ top: "40%", position: "absolute", width: "100%" }}>
                                                <h3 style={{ color: "#9ACB62" }}> {this.state.unitTests.passed} </h3>
                                                <h3>&nbsp;|&nbsp;</h3>
                                                <h3 style={{ color: "#C25149" }}> {this.state.unitTests.failed} </h3>
                                            </div>

                                        </div>

                                        <canvas id="codeCoverageChart" height="300"></canvas>

                                    </div>
                                    
                                    <div id="endGap" style={{ width: "100%", flexGrow:"1"}}></div>
                                </Grid>
                            </Grid>

                            {/* Right Tests Container */}
                            <Grid container direction="column" item id="rightTestsContainer">
                                {this.state.rightTests.map((test) => {
                                    return <TestsPieChart test={test} key={test.type}></TestsPieChart>
                                })}
                            </Grid>

                        </Paper>
                    </Grid>
                                
                    {/* Bugs Container */}
                    <Grid id="bugsGrid" item xs={12} sm={6} md={4} lg={2} xl={2}>
                        <Paper id="bugsPaper">
                            <div className="bugs" style={{ float: "right", display: "block" }}>
                                <p className="widgetTitle" style={{ paddingBottom: "2rem", paddingRight:"1rem" }}>באגים</p>
                            </div>
                            <div id="bugsChartContainer" style={{ width: "90%", height: "85%", paddingTop: "20%", paddingBottom: "20%", alignItems: "right" }}>
                                <canvas id="bugsChart" style={{ height: "100%", width: "90%" }}></canvas>
                            </div>
                        </Paper>
                    </Grid >
                </Grid>
            </div >
        );
    }
}

export default GafQAPage;

