import React, { Component } from 'react';
import './GafSummaryPage.css';
import Grid from '@material-ui/core/Grid';
import { Paper, LinearProgress } from '@material-ui/core';
import LastBuildsSummary from '../../../components/LastBuildsSummary/LastBuildsSummary';
import LastCriticalBugs from '../../../components/LastCriticalBugs/LastCriticalBugs';
import ScheduleIcon from '@material-ui/icons/Schedule';

class GafSummaryPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            sprintCompleted: 90,
            daysLeft: 3,

        }
    }

    fillProgressBar() {
        return this.props.sprint && this.props.sprint.sprint_passed_time_percent ? this.props.sprint.sprint_passed_time_percent : this.state.sprintCompleted
    }

    render() {
        return (
            <div className="tabPage">
                <Grid item container xs={11} lg={11} direction="row" className="widgetsSection">
                    <Grid item className="sprintStatusWidget" xs={12} lg={12}>
                        <Paper className="widgetPaper">
                            <div className="widgetDiv" id="sprintDaysDiv">
                                <p className="widgetTitle" id="sprintDays">נותרו <p id="numOfDaysLeft">{this.props.sprint ? this.props.sprint.sprint_remaining_time : this.state.daysLeft}</p> ימים לספרינט</p>
                                <LinearProgress id="progressBarSprint" variant="determinate" value={this.fillProgressBar()}></LinearProgress>
                            </div>
                        </Paper>
                    </Grid>

                    <Grid item container direction="row" className="lowSectionOfPage" xs={12} lg={12}>
                        <Grid item id="monitorSummaryWidget" xs={12} md={6} lg={4} className="summaryWidget" style={{ maxWidth: "100%" }}>
                            <Paper className="widgetPaper">
                                <div className="widgetDiv">
                                    <div className="summaryIconDiv"><ScheduleIcon id="clockIcon"></ScheduleIcon></div>
                                    <div className="widgetTitle summary-title-padding">
                                        כאן יופיעו בעתיד חריגות ניטור
                                    </div>
                                    <img src={require('../../../assets/brain.png')} alt="" style={{ marginLeft: "0", width: "100%", height: "55%" }} />                                </div>
                            </Paper>
                        </Grid>
                        <LastCriticalBugs criticalBugs={this.props.criticalBugs}></LastCriticalBugs>
                        <LastBuildsSummary services={this.props.lastPlanBuildsInfo.services}></LastBuildsSummary>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default GafSummaryPage;
