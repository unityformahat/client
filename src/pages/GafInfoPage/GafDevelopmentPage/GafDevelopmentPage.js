import React, { Component } from "react";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import Grid from "@material-ui/core/Grid";
import "./GafDevelopmentPage.css";
import { Paper } from "@material-ui/core";
import ServiceDataRow from "../../../components/ServiceDataRow/ServiceDataRow.js";
import AddServiceForm from "../../../components/AddServiceForm/AddServiceForm";

class GafDevelopmentPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // buildsInfo: props.buildsInfo,
      tempBuildsInfo: {
        plans: [
          {
            planname: "רכיב 1",
            buildsresults: [
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: false,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: false,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
            ],
          },
          {
            planname: "רכיב 2",
            buildsresults: [
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: false,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: false,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
            ],
          },
          {
            planname: "רכיב 3",
            buildsresults: [
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: false,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: false,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
            ],
          },
          {
            planname: "רכיב 4",
            buildsresults: [
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: false,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: false,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
            ],
          },
          {
            planname: "רכיב 5",
            buildsresults: [
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: false,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: false,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
            ],
          },
          {
            planname: "רכיב 6",
            buildsresults: [
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: false,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: false,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
              {
                buildstarttime: Date.now(),
                buildstate: true,
                faildtestcount: 5,
                successfultestcount: 9,
                buildername: "אלון",
              },
            ],
          },
        ],
      },
      titles: [
        {
          title: "שם הרכיב",
          xsSize: 2,
          smSize: 2,
          style: { paddingLeft: "2%" },
        },
        {
          title: "בניה אחרונה",
          xsSize: 4,
          smSize: 4,
          style: { paddingLeft: "2%" },
        },
        {
          title: "קומיטים של בקשת מיזוג אחרונה",
          xsSize: 3,
          smSize: 3,
          style: {},
        },
        {
          title: "בקשות מיזוג פתוחות למאסטר",
          xsSize: 3,
          smSize: 3,
          style: { paddingRight: "2%" },
        },
      ],
      pullRequestsInfo: [
        {
          repoid: 1,
          reponame: "myrepo1",
          open_pull_requests_to_master: [
            {
              create_date: "2020-05-12",
              developername: "אלון",
              frombranchname: "my-new-branch",
            },
            {
              create_date: "2020-05-04",
              developername: "אלון",
              frombranchname: "first_branch",
            },
          ],
        },
        {
          repoid: 2,
          reponame: "myrepo2",
          open_pull_requests_to_master: [
            {
              create_date: "2020-05-12",
              developername: "גיא",
              frombranchname: "my-new-branch",
            },
            {
              create_date: "2020-05-04",
              developername: "גיא",
              frombranchname: "first_branch",
            },
            {
              create_date: "2020-05-04",
              developername: "גיא",
              frombranchname: "first_branch",
            },
            {
              create_date: "2020-05-04",
              developername: "גיא",
              frombranchname: "first_branch",
            },
          ],
        },
        {
          repoid: 3,
          reponame: "myrepo3",
          open_pull_requests_to_master: [
            {
              create_date: "2020-05-12",
              developername: "הדס",
              frombranchname: "my-new-branch",
            },
            {
              create_date: "2020-05-04",
              developername: "הדס",
              frombranchname: "first_branch",
            },
            {
              create_date: "2020-05-04",
              developername: "הדס",
              frombranchname: "first_branch",
            },
            {
              create_date: "2020-05-04",
              developername: "הדס",
              frombranchname: "first_branch",
            },
            {
              create_date: "2020-05-04",
              developername: "הדס",
              frombranchname: "first_branch",
            },
            {
              create_date: "2020-05-04",
              developername: "הדס",
              frombranchname: "first_branch",
            },
          ],
        },
        {
          repoid: 4,
          reponame: "myrepo4",
          open_pull_requests_to_master: [
            {
              create_date: "2020-05-12",
              developername: "יובל",
              frombranchname: "my-new-branch",
            },
            {
              create_date: "2020-05-04",
              developername: "יובל",
              frombranchname: "first_branch",
            },
            {
              create_date: "2020-05-04",
              developername: "יובל",
              frombranchname: "first_branch",
            },
          ],
        },
        {
          repoid: 5,
          reponame: "myrepo5",
          open_pull_requests_to_master: [
            {
              create_date: "2020-05-12",
              developername: "רונן",
              frombranchname: "my-new-branch",
            },
            {
              create_date: "2020-05-04",
              developername: "רונן",
              frombranchname: "first_branch",
            },
          ],
        },
        {
          repoid: 6,
          reponame: "myrepo6",
          open_pull_requests_to_master: [
            {
              create_date: "2020-05-12",
              developername: "ירין",
              frombranchname: "my-new-branch",
            },
            {
              create_date: "2020-05-04",
              developername: "ירין",
              frombranchname: "first_branch",
            },
          ],
        },
      ],
      commitsInfo: [
        {
          repoid: 1,
          reponame: "my new repo",
          commits: [
            "finished summary page finally",
            "qa page fixed bug",
            "changed style of welcome page",
            "finished summary page finally finished summary page finally finished summary page finally finished summary page finally",
            "finished summary page finally",
          ],
          pull_request_date: "2020-05-14",
        },
        {
          repoid: 2,
          reponame: "my new repo",
          commits: [
            "finished summary page finally",
            "qa page fixed bug",
            "changed style of welcome page",
            "finished summary page finally",
            "finished summary page finally",
          ],
          pull_request_date: "2020-05-14",
        },
        {
          repoid: 3,
          reponame: "my new repo",
          commits: [
            "finished summary page finally",
            "qa page fixed bug",
            "changed style of welcome page",
            "finished summary page finally",
            "finished summary page finally",
            "finished summary page finally",
            "finished summary page finally",
            "finished summary page finally",
          ],
          pull_request_date: "2020-05-14",
        },
        {
          repoid: 4,
          reponame: "my new repo",
          commits: [
            "finished summary page finally",
            "qa page fixed bug",
            "changed style of welcome page",
            "finished summary page finally",
            "finished summary page finally",
          ],
          pull_request_date: "2020-05-14",
        },
        {
          repoid: 5,
          reponame: "my new repo",
          commits: [
            "finished summary page finally",
            "qa page fixed bug",
            "changed style of welcome page",
            "finished summary page finally",
            "finished summary page finally",
          ],
          pull_request_date: "2020-05-14",
        },
        {
          repoid: 6,
          reponame: "my new repo",
          commits: [
            "finished summary page finally",
            "qa page fixed bug",
            "changed style of welcome page",
            "finished summary page finally",
            "finished summary page finally",
          ],
          pull_request_date: "2020-05-14",
        },
      ],
      servicesData: [
        {
          lastCommitMessage: "finished summary page finally",
          lastCommiterToMasterName: "אלון כהנא",
          lastCommitToMasterDate: "12/11/19",
          lastCommitToMasterMessage: "finished summary page finally",
          lastPullRequestToMasterName: "הדס משה",
          lastPullRequestToMasterDate: "12/11/19",
          lastPullRequestToMasterSourceBranch: "summary_page",
        },
        {
          lastCommitMessage: "qa page fixed bug",
          lastCommiterToMasterName: "הדס משה",
          lastCommitToMasterDate: "12/11/19",
          lastCommitToMasterMessage: "qa page fixed bug",
          lastPullRequestToMasterName: "גיא שר",
          lastPullRequestToMasterDate: "12/11/19",
          lastPullRequestToMasterSourceBranch: "qa_page",
        },
        {
          lastCommitMessage: "changed style of welcome page",
          lastCommiterToMasterName: "גיא שר",
          lastCommitToMasterDate: "12/11/19",
          lastCommitToMasterMessage: "changed style of welcome page",
          lastPullRequestToMasterName: "אלון כהנא",
          lastPullRequestToMasterDate: "12/11/19",
          lastPullRequestToMasterSourceBranch: "menu_bar",
        },
        {
          lastCommitMessage: "finished summary page finally",
          lastCommiterToMasterName: "גיא שר",
          lastCommitToMasterDate: "12/11/19",
          lastCommitToMasterMessage: "changed style of welcome page",
          lastPullRequestToMasterName: "אלון כהנא",
          lastPullRequestToMasterDate: "12/11/19",
          lastPullRequestToMasterSourceBranch: "menu_bar",
        },
        {
          lastCommitMessage: "finished summary page finally",
          lastCommiterToMasterName: "גיא שר",
          lastCommitToMasterDate: "12/11/19",
          lastCommitToMasterMessage: "changed style of welcome page",
          lastPullRequestToMasterName: "אלון כהנא",
          lastPullRequestToMasterDate: "12/11/19",
          lastPullRequestToMasterSourceBranch: "menu_bar",
        },
      ],
    };
  }
  componentDidMount() {}

  getServiceIndex(buildIndex) {
    return buildIndex % 5;
  }

  render() {
    return (
      <div className="tabPage">
        <Grid
          item
          container
          xs={11}
          direction="column"
          id="developmentPageContentContainer"
          className="widgetsSection black-scroll-bar"
        >
          {/* Titles grid */}
          <Grid item xs={1} container id="developmentPageTitlesContainer">
            {this.state.titles.map((title) => (
              <Grid
                item
                key={title.title}
                xs={title.xsSize}
                sm={title.smSize}
                style={title.style}
              >
                <Paper className="titlePaper">
                  <div className="widgetdiv" id="title">
                    <p className="widgetTitle-small">{title.title}</p>
                  </div>
                </Paper>
              </Grid>
            ))}
          </Grid>

          <Grid
            item
            xs={10}
            container
            direction="row"
            id="ServicesInfoContainer"
            className="black-scroll-bar"
          >
            {/* Create Builds Info Rows (map one service data to one row)  */}
            {this.state.tempBuildsInfo ? (
              <GridList
                cellHeight={180}
                id="DevelopmentRowsGridList"
                className="DevelopmentRows"
              >
                <div className="add-service-form">
                  <AddServiceForm />
                </div>
                {this.props.buildsInfo.plans.length > 0
                  ? this.props.buildsInfo.plans.map((build, buildIndex) => (
                      <GridListTile
                        key={build.planname}
                        cols={2}
                        id="gridListTile"
                      >
                        <ServiceDataRow
                          key={build.planname}
                          index={buildIndex}
                          commitsInfo={this.props.commitsInfo[buildIndex]}
                          pullRequestsInfo={
                            this.props.pullRequestsInfo[buildIndex]
                          }
                          serviceInfo={
                            this.state.servicesData[
                              this.getServiceIndex(buildIndex)
                            ]
                          }
                          buildInfo={build}
                        ></ServiceDataRow>
                      </GridListTile>
                    ))
                  : this.state.tempBuildsInfo.plans.map((build, buildIndex) => (
                      <GridListTile
                        key={build.planname}
                        cols={2}
                        id="gridListTile"
                      >
                        <ServiceDataRow
                          key={build.planname}
                          index={buildIndex}
                          commitsInfo={this.state.commitsInfo[buildIndex]}
                          pullRequestsInfo={
                            this.state.pullRequestsInfo[buildIndex]
                          }
                          serviceInfo={
                            this.state.servicesData[
                              this.getServiceIndex(buildIndex)
                            ]
                          }
                          buildInfo={build}
                        ></ServiceDataRow>
                      </GridListTile>
                    ))}
              </GridList>
            ) : (
              ""
            )}
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default GafDevelopmentPage;
