import React, { Component } from 'react';
import './GafManagmentPage.css';
import SprintHealth from '../../../components/SprintHealth/SprintHealth';
import TasksProgressDiagram from '../../../components/TasksProgressDiagram/TasksProgressDiagram'
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Capacity from '../../../components/Capacity/Capacity.js'
import ReportedGafHoursTable from '../../../components/ReportedGafHoursTable/ReportedGafHoursTable.js'

class GafManagmentPage extends Component {
    render() {
        return (
            <div className="tabPage">
                <Grid item container xs={11} direction="column" className="widgetsSection">
                    <Grid item container direction="row" className="upperSection">
                        <Grid item xs={12} lg={8} className="sprintHealth">
                            <Paper id="sprintHealthPaper" className="widgetPaper">
                                <div className="widgetDiv flexed-div">
                                    <div>
                                        <p className="widgetTitle" style={{ paddingBottom: "2rem" }}>בריאות הספרינט</p>
                                    </div>
                                    <Grid item container xs={12} id="managementChartsContainer" direction="row">
                                        <Grid item sm={12} md={6} id="TasksProgressDiagramItem">
                                            <TasksProgressDiagram JiraData={this.props.issuesStatus} style={{ position: "absolute" }} />
                                        </Grid>
                                        <Grid item sm={12} md={6} id="sprintHealthItem">
                                            <SprintHealth />
                                        </Grid>
                                    </Grid>
                                </div>
                            </Paper>
                        </Grid>

                        <Grid item xs={12} lg={4} id="capacity">

                            <Paper className="widgetPaper">

                                <div className="widgetDiv">
                                    <div>
                                        <p className="widgetTitle" style={{ paddingBottom: "2rem" }}> קיבולת </p>
                                    </div>
                                    <Capacity capacityPercentage={90} />
                                </div>
                            </Paper>
                        </Grid>
                    </Grid>
                    <Grid item container direction="row" className="lowerSection">
                        <Grid item xs={12} style={{ height: "100%" }}>
                            <Paper className="widgetPaper" style={{ marginTop: "2%", height: "92%" }}>
                                <ReportedGafHoursTable data={this.props.workReports} />
                            </Paper>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        );
    }
}
export default GafManagmentPage;