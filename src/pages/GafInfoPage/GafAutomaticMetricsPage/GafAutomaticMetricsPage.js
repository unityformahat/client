import React, { Component } from 'react';
import PageUnderConstruction from '../../../components/PageUnderConstruction/PageUnderConstruction';
import './GafAutomaticMetricsPage.css';

class GafAutomaticMetricsPage extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }
    render() {
        return (
            <PageUnderConstruction pageName={"מדדים אוטומטיים"}></PageUnderConstruction>
        );
    }
}

export default GafAutomaticMetricsPage;
