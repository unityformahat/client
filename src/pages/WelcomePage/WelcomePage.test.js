import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import pretty from "pretty";

import WelcomePage from "./WelcomePage";

let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("should render the page", () => {
  act(() => {
    render(<WelcomePage />, container);
  });
  expect(pretty(container.innerHTML)).toMatchInlineSnapshot(`
    "<div class=\\"WelcomePage\\">
      <h5 class=\\"MuiTypography-root-1 MuiTypography-h5-17\\" id=\\"unity-home-page-part1\\">Welcome to</h5>
      <h5 class=\\"MuiTypography-root-1 MuiTypography-h5-17\\" id=\\"unity-home-page-part2\\">Unity</h5>
      <h5 class=\\"MuiTypography-root-1 MuiTypography-h5-17\\" id=\\"unity-home-page-part3\\">Dashboards</h5>
      <div class=\\"MuiGrid-container-37 MuiGrid-item-38\\" id=\\"cardsContainer\\">
        <div class=\\"MuiGrid-item-38 MuiGrid-grid-xs-10-75 MuiGrid-grid-sm-6-85 MuiGrid-grid-md-2-95\\">
          <div class=\\"Card\\">
            <div class=\\"img\\"><img class=\\"logo\\" src=\\"/assets/logo4.JPG\\" alt=\\"\\"></div>
            <header>
              <h1 class=\\"card-head\\">תהנו מהדרך</h1>
            </header>
            <div>
              <p class=\\"card-body\\">התמידו וצברו נקודות, התחרו עם שאר הצוותים ביחידה והכניסו כיף ליום העבודה</p>
            </div>
          </div>
        </div>
        <div class=\\"MuiGrid-item-38 MuiGrid-grid-xs-10-75 MuiGrid-grid-sm-6-85 MuiGrid-grid-md-2-95\\">
          <div class=\\"Card\\">
            <div class=\\"img\\"><img class=\\"logo\\" src=\\"/assets/logo3.JPG\\" alt=\\"\\"></div>
            <header>
              <h1 class=\\"card-head\\">כוונו גבוה</h1>
            </header>
            <div>
              <p class=\\"card-body\\">השתמשו במערכת על מנת לשפר את תהליכי הפיתוח ולהגיע הכי רחוק שרק אפשר</p>
            </div>
          </div>
        </div>
        <div class=\\"MuiGrid-item-38 MuiGrid-grid-xs-10-75 MuiGrid-grid-sm-6-85 MuiGrid-grid-md-2-95\\">
          <div class=\\"Card\\">
            <div class=\\"img\\"><img class=\\"logo\\" src=\\"/assets/logo1.JPG\\" alt=\\"\\"></div>
            <header>
              <h1 class=\\"card-head\\">הישארו בשליטה</h1>
            </header>
            <div>
              <p class=\\"card-body\\"> תעזור לכם להישאר בשליטה Unity לאורך כל תהליך הפיתוח השלם ולשלכם</p>
            </div>
          </div>
        </div>
        <div class=\\"MuiGrid-item-38 MuiGrid-grid-xs-10-75 MuiGrid-grid-sm-6-85 MuiGrid-grid-md-2-95\\">
          <div class=\\"Card\\">
            <div class=\\"img\\"><img class=\\"logo\\" src=\\"/assets/logo2.JPG\\" alt=\\"\\"></div>
            <header>
              <h1 class=\\"card-head\\">בלי להסתבך</h1>
            </header>
            <div>
              <p class=\\"card-body\\"> המידע מוצג בפשטות על מנת להקל ולקצר את הזמן הדרוש לניהול הצוותים</p>
            </div>
          </div>
        </div>
      </div>
    </div>"
  `);
});
