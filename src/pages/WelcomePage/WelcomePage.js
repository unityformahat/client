import React, { Component } from 'react';
import './WelcomePage.css';
import Card from '../../components/WelcomeCard/Card';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';

class WelcomePage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      cardsInfo: [
        {
          imagePath: "/assets/logo4.JPG",
          header: "תהנו מהדרך",
          content: "התמידו וצברו נקודות, התחרו עם שאר הצוותים ביחידה והכניסו כיף ליום העבודה"
        },
        {
          imagePath: "/assets/logo3.JPG",
          header: "כוונו גבוה",
          content: "השתמשו במערכת על מנת לשפר את תהליכי הפיתוח ולהגיע הכי רחוק שרק אפשר"
        },
        {
          imagePath: "/assets/logo1.JPG",
          header: "הישארו בשליטה",
          content: " תעזור לכם להישאר בשליטה Unity לאורך כל תהליך הפיתוח השלם ולשלכם"
        },
        {
          imagePath: "/assets/logo2.JPG",
          header: "בלי להסתבך",
          content: " המידע מוצג בפשטות על מנת להקל ולקצר את הזמן הדרוש לניהול הצוותים"
        },

      ]
    }
  }
  render() {
    return (
      <div className="WelcomePage">
        <Typography variant="h5" id="unity-home-page-part1">Welcome to</Typography>
        <Typography variant="h5" id="unity-home-page-part2">Unity</Typography>
        <Typography variant="h5" id="unity-home-page-part3">Dashboards</Typography>
          <Grid item container  direction='row' id="cardsContainer">
            {this.state.cardsInfo.map((cardInfo, key) => {
              return <Grid item xs={10} sm={6} md={2}>
                    <Card cardInfo={cardInfo}></Card>
              </Grid>
            })}
          </Grid>
      </div>
    );
  }
}

export default WelcomePage;
