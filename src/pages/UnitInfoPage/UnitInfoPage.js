import React, { Component } from 'react';
import BreadCrumbs from '../../components/BreadCrumbs/BreadCrumbs.js';
import { withRouter } from 'react-router-dom';

class UnitInfoPage extends Component {
    constructor(props) {
        super(props);



        this.state = {
            units: []
        }
    }

    isExist() {
        var unitName = this.props.match.params.unit;
        var boolExist = this.state.units.find(unit => {
            return unit.unit_name === unitName;
        });
        return boolExist
    }

    componentDidMount = () => {
        fetch('/api/hierarchy')
            .then(res => res.json())
            .then(data => this.setState({ units: data }))
            .catch(console.log)
    }

    render() {
        return (<div>

            {this.isExist() ?
                <React.Fragment>
                    <BreadCrumbs></BreadCrumbs>
                    <p>{this.props.match.params.unit}</p>
                </React.Fragment>
                :
                <p>היחידה איננה קיימת במערכת </p>}
        </div>)




    }
}

export default withRouter(UnitInfoPage);
