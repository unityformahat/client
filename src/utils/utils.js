    /**
     * handelTextChange reconginzes a change in the text field and in a result changes the following states:
     * @param validate_"id" = state for if the input is valid 
     * @param errorText = the error message
     * @param event.target.id = the actuall info in the field saved as a state
     * handelTextChange get those params:
     * @param event = the event of the text change
     * @param klass = klass is representing "this" so we can edit state
     */
 
  export function handleTextChange(event, klass)  {
    // checking if user inserted anything that is not hebrew letters or spaces
    // if the user inserted legal argument
    if (event.target.value.match(/^([\u0590-\u05FF ]{3,20})$/g)) {
      klass.setState({
        [event.target.id]: event.target.value,
        errorText: "",
        ["validate_" + event.target.id]: true,
      });
      // this part is not working for some reasson, should move you to the next field if you press enter.
      // if (event.key === "Enter") {
      //   this.handleNext();
      // }
      // if the user inserted ilegal argument
    } else {
      klass.setState({
        [event.target.id]: event.target.value,
        errorText: "פורמט לא תקין: רק אותיות בעברית ",
        ["validate_" + event.target.id]: false,
      });
    }
  };


      /**
     * handleSelectChange reconginzes a change in the select field and in a result activating handelnext if existed,
     * and changes the following states:
     * @param validate_"id" = state for if the input is valid 
     * @param event.target.id = the actuall info in the field saved as a state
     * handleSelectChange get those params:
     * @param event = the event of the text change
     * @param klass = klass is representing "this" so we can edit state
     * @param id = the id of the statement and the component
     */
  export function handleSelectChange (event, klass, id)  {
    // checking if the user chose something from the list that is no the default value
    if (event.target.value !== 0) {
      klass.setState({
        [id]: event.target.value,
        ["validate_" + id]: true,
      });
      if (typeof klass.handleNext === "function"){
      klass.handleNext();}
    } else {
      klass.setState({
        [event.target.id]: event.target.value,
        ["validate_" + event.target.id]: false,
      });
    }
  };