// an action creator that creates an action that describe a selected unit to display

export const SELECT_UNIT = 'SELECT_UNIT'

export function selectUnit(unit) {
    return {
        type: SELECT_UNIT,
        unit
    }
}

// an action creator that creates an action that describe a selected gaf to display

export const RECIEVE_GAF = 'RECIEVE_GAF'

export function recieveGaf(unit, gaf) {
    return {
        type: RECIEVE_GAF,
        unit,
        gaf
    }
}

export function selectGaf(unitID, gafID) {
    return function (dispatch, getState) {
        var state = getState()
        var selectedUnit = state.hierarchy.units.find(unit => unit.unit_id == unitID);
        var selectedGaf = selectedUnit.gafs.find(gaf => gaf.gaf_id == gafID)
        // send data to store
        return dispatch(recieveGaf(selectedUnit, selectedGaf))
    }

}

// an action creator that creates an action that indicates the gaf data is not describe a way to refresh the gaf data to display

export const INVALIDATE_GAF = 'INVALIDATE_GAF'

export function invalidateGaf(gaf) {
    return {
        type: INVALIDATE_GAF,
        gaf
    }
}

// an action creator that creates an action that describe a request to fetch builds data

export const REQUEST_BUILDS = 'REQUEST_BUILDS'

export function requestBuilds(gaf) {
    return {
        type: REQUEST_BUILDS,
        gaf
    }
}

// a function that sorts plan data

function sortPlans(data) {
    if (data.plans) {
        var sortedBuilds = data
        sortedBuilds.plans.sort(function (a, b) {
            if (a.buildsresults[0] && b.buildsresults[0]) {
                return new Date(b.buildsresults[0].buildstarttime) - new Date(a.buildsresults[0].buildstarttime);
            } else {
            }
            return 0;
        })
        return sortedBuilds;
    }
}

// an action creator that creates an action that describe a succesfuly fetched builds data

export const RECIEVE_BUILDS = 'RECIEVE_BUILDS'

export function recieveBuilds(gaf, json) {
    var sortedBuilds = sortPlans(json);
    return {
        type: RECIEVE_BUILDS,
        gaf,
        builds: sortedBuilds,
        recivedAt: Date.now()
    }
}

function fetchBuilds(unit, gaf) {
    return function (dispatch) {
        // update app state to inform that the api call is starting
        dispatch(requestBuilds(gaf));
        // fetch data from API
        return fetch('/api/ofek/' + unit + '/' + gaf).then(
            response => response.json())
            .then(
                // update the app state with the results of the API call
                json => dispatch(recieveBuilds(gaf, json))
            );
    }

}

function shouldFetchBuilds(state, gaf) {
    const builds = state.buildsOfGaf[gaf];
    if (!builds) {
        return true;
    }
    else if (builds.isFetching) {
        return false;
    }
    else {
        if (builds.didInvalidate === false) {
            console.log("already fetched gaf" + gaf + " at " + builds.lastUpdated);
        }
        return builds.didInvalidate
    }
}

export function fetchBuildsIfNeeded(unit, gaf) {
    return (dispatch, getState) => {
        if (shouldFetchBuilds(getState(), gaf)) {
            return dispatch(fetchBuilds(unit, gaf));
        }
        else {
            return Promise.resolve()
        }
    }
}

///////////////////////////////////////// Gaf information //////////////////////////////////////

// an action creator that creates an action that describe a request to fetch gaf information

export const REQUEST_GAF_INFORMATION = 'REQUEST_GAF_INFORMATION'

export function requestGafInformation(unit, gaf) {
    return {
        type: REQUEST_GAF_INFORMATION,
        unit,
        gaf
    }
}

// an action creator that creates an action that describe a succesfuly fetched gaf information

export const RECIEVE_GAF_INFORMATION = 'RECIEVE_GAF_INFORMATION'

export function recieveGafInformation(unit, gaf, json) {
    return {
        type: RECIEVE_GAF_INFORMATION,
        unit,
        gaf,
        newInfo: json,
        recivedAt: Date.now()
    }
}

// an action creator that creates an action that indicates the gaf data is not describe a way to refresh the gaf data to display

export const INVALIDATE_GAF_INFORMATION = 'INVALIDATE_GAF_INFORMATION'

export function invalidateGafInformation(unit, gaf) {
    return {
        type: INVALIDATE_GAF_INFORMATION,
        unit,
        gaf
    }
}

// a function that fetches the gaf data
function fetchGafInformation(unit, gaf, dataToFetch) {
    return async function (dispatch) {
        // update app state to inform that the api call is starting
        dispatch(requestGafInformation(unit, gaf));
        // fetch data from API
        var json = {};
        await fetchDataForGaf(gaf, json, dataToFetch);
        return dispatch(recieveGafInformation(unit, gaf, json));
    }
}

async function fetchDataForGaf(gaf, json, dataToFetch) {
    return Promise.all(
        dataToFetch.map(async dataType => {
            await fetch('/api/' + dataType + '/' + gaf).then(response => response.json().then(res => {return json[dataType] = res}));
        })
    )
}


function getUnitIndex(unitsArray, unitID) {
    var i=0;
    while (i < unitsArray.length) {
        if (unitsArray[i].unit_id === unitID) {
            return i;
        }
        i+=1;
    }
    return -1;
}

function getGafIndex(gafsArray, gafID) {
    var i=0;
    while (i < gafsArray.length) {
        if (gafsArray[i].gaf_id === gafID) {
            return i;
        }
        i+=1;
    }
    return -1;
}

// a function that validate the gaf information and returns if fetch is needed
function shouldFetchGafInformation(state, unit, gaf) {
    const unitIndex = getUnitIndex(state.hierarchy.units, unit);
    const gafIndex = getGafIndex(state.hierarchy.units[unitIndex].gafs, gaf);
    const gafInformation = state.hierarchy.units[unitIndex].gafs[gafIndex].gafInformation;
    var shouldFetch = false;
    var dataToFetch = [];
    if (Object.entries(gafInformation).length === 0) {
        shouldFetch = true;
        dataToFetch.push("sprint");
        dataToFetch.push("critical_bugs");
        dataToFetch.push("bug_count");
        dataToFetch.push("stories_status_data");
        dataToFetch.push("work_reports");
        dataToFetch.push("last_build_for_plan");        
        dataToFetch.push("last_builds");
        dataToFetch.push("opened_pull_requests");
        dataToFetch.push("commits");
    }
    if (gafInformation.isFetching) {
        shouldFetch = false;
    }
    else if (gafInformation.didInvalidate) {
            console.log("need to fetch gaf" + gaf);
    }
    else if (gafInformation.gafId && gaf !== gafInformation.gafId) {
        shouldFetch = true;
        dataToFetch.push("sprint");
        dataToFetch.push("critical_bugs");
        dataToFetch.push("bug_count");
        dataToFetch.push("stories_status_data");
        dataToFetch.push("work_reports");
        dataToFetch.push("last_build_for_plan");
        dataToFetch.push("last_builds");
        dataToFetch.push("opened_pull_requests");
        dataToFetch.push("commits");
    }
    return {
        "shouldFetch": shouldFetch,
        "dataToFetch": dataToFetch
    }
}

// a function that calls the validation function and then calls the fetch function
export function fetchGafInformationIfNeeded(unit, gaf) {
    return (dispatch, getState) => {
        const fetch = shouldFetchGafInformation(getState(), unit, gaf);
        if (fetch.shouldFetch) {
            return dispatch(fetchGafInformation(unit, gaf, fetch.dataToFetch));
        }
        else {
            return Promise.resolve()
        }
    }
}

/////////////////////////////////////// Hierarchy ///////////////////////////////////////////

// an action creator that creates an action that describe a request to fetch hierarchy data

export const REQUEST_HIERARCHY = 'REQUEST_HIERARCHY'

export function requestHierarchy() {
    return {
        type: REQUEST_HIERARCHY,
    }
}

// an action creator that creates an action that describe a succesfuly fetched hierarchy data

export const RECIEVE_HIERARCHY = 'RECIEVE_HIERARCHY'

export function recieveHierarchy(unitsArray) {
    var hierarchyWithInformationArray = unitsArray;
    hierarchyWithInformationArray.map(unit => {
        unit.gafs.map(gaf => {
            gaf.gafInformation = {}
        })
    })
    return {
        type: RECIEVE_HIERARCHY,
        hierarchy: hierarchyWithInformationArray,
        recivedAt: Date.now()
    }
}

// an action creator that creates an action that indicates the gaf data is not describe a way to refresh the gaf data to display

export const INVALIDATE_HIERARCHY = 'INVALIDATE_HIERARCHY'

export function invalidateHierarchy() {
    return {
        type: INVALIDATE_HIERARCHY
    }
}

// a function that fetches the hierarchy data
function fetchHierarchy() {
    return function (dispatch) {
        // update app state to inform that the api call is starting
        dispatch(requestHierarchy());
        // fetch data from API
        return fetch('/api/hierarchy').then(
            response => response.json())
            .then(
                // update the app state with the results of the API call
                json => dispatch(recieveHierarchy(json.units))
            );
    }
}

// a function that validate the hierarchy information and returns if fetch is needed
function shouldFetchHierarchy(state) {
    const hierarchy = state.hierarchy;

    // if information does not exist
    if (0 === hierarchy.units.length) {
        return true;
    }
    // if fetching right now
    else if (hierarchy.isFetching) {
        return false;
    }
    // validation check
    else {
        if (hierarchy.didInvalidate === false) {
            console.log("already fetched hierarchy at " + hierarchy.lastUpdated);
        }
        return hierarchy.didInvalidate
    }
}

// a function that calls the validation function and then calls the fetch function
export function fetchHierarchyIfNeeded() {
    return (dispatch, getState) => {
        if (shouldFetchHierarchy(getState())) {
            return dispatch(fetchHierarchy());
        }
        else {
            return Promise.resolve()
        }
    }
}

/////////////////////////////////////// All the Sprints ///////////////////////////////////////////

// an action creator that creates an action that describe a request to fetch sprints

export const REQUEST_SPRINTS = 'REQUEST_SPRINTS'

export function requestSprints() {
    return {
        type: REQUEST_SPRINTS
    }
}

// an action creator that creates an action that describe a succesfuly fetched sprints

export const RECIEVE_SPRINTS = 'RECIEVE_SPRINTS'

export function recieveSprints(json) {
    return {
        type: RECIEVE_SPRINTS,
        sprints: json,
        recivedAt: Date.now()
    }
}

// an action creator that creates an action that indicates the sprints data is not describe a way to refresh the sprints data to display

export const INVALIDATE_SPRINTS = 'INVALIDATE_SPRINTS'

export function invalidateSprints() {
    return {
        type: INVALIDATE_SPRINTS
    }
}

// a function that fetches the sprints data
function fetchSprints() {
    return function (dispatch) {
        // update app state to inform that the api call is starting
        dispatch(requestSprints());
        // fetch data from API
        return fetch('/api/sprints').then(
            response => response.json())
            .then(
                // update the app state with the results of the API call
                json => dispatch(recieveSprints(json))
            );
    }
}

// a function that validate the sprints information and returns if fetch is needed
function shouldFetchSprints(state) {
    const sprints = state.sprints;

    // if information does not exist
    if (0 === sprints.length) {
        return true;
    }
    // if fetching right now
    else if (sprints.isFetching) {
        return false;
    }
    // validation check
    else {
        if (sprints.didInvalidate === false) {
            console.log("already fetched sprints at " + sprints.lastUpdated);
        }
        return sprints.didInvalidate
    }
}

// a function that calls the validation function and then calls the fetch function
export function fetchSprintsIfNeeded() {
    return (dispatch, getState) => {
        if (shouldFetchSprints(getState())) {
            return dispatch(fetchSprints());
        }
        else {
            return Promise.resolve()
        }
    }
}
