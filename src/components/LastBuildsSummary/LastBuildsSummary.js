import React, { Component } from 'react';
import './LastBuildsSummary.css';
import Grid from '@material-ui/core/Grid';
import ServiceBuildsResults from '../ServiceBuildsResults/ServiceBuildsResults';
import { Paper } from '@material-ui/core';
import BuildIcon from '@material-ui/icons/Build';

class lastBuildsSummary extends Component {
    constructor (props) {
        super(props)
        this.state = {
            
            services: [
                {
                    key: 1,
                    serviceName: "רכיב 1",
                    result: "failed",
                },
                {
                    key: 2,
                    serviceName: "רכיב 2",
                    result: "failed",
                },
                {
                    key: 3,
                    serviceName: "רכיב 3",
                    result: "failed",
                },
                {
                    key: 4,
                    serviceName: "רכיב 4",
                    result: "success",
                },
                {
                    key: 5,
                    serviceName: "רכיב 5",
                    result: "success",
                },
                {
                    key: 6,
                    serviceName: "רכיב 6",
                    result: "success",
                },
                {
                    key: 7,
                    serviceName: "רכיב 7",
                    result: "success",
                },
                {
                    key: 8,
                    serviceName: "רכיב 8",
                    result: "success",
                },
                {
                    key: 9,
                    serviceName: "רכיב 9",
                    result: "success",
                },
                {
                    key: 10,
                    serviceName: "רכיב 10",
                    result: "success",
                },
                {
                    key: 11,
                    serviceName: "רכיב 11",
                    result: "success",
                },
            ]
        }
    }
    render() {
        return (
            <Grid item id="lastBuildsSummaryWidget" xs={12} lg={4} className="summaryWidget">
            <Paper className="widgetPaper">
                <div className="widgetDiv flexed-div">
                    <div className="summaryIconDiv">
                        <BuildIcon id="buildIcon" fontSize="large" />
                    </div>
                    <div>
                        <p id="buildsStatusTitle" className="widgetTitle summary-title-padding">תוצאות הבניות האחרונות</p>
                    </div>
                    <div id="serviceNumDiv">
                        <p id="servicesNum" className="summary-title-padding">מתוך {this.props.services.length > 0 ? this.props.services.length : this.state.services.length} רכיבים</p>
                    </div>
                    <div className="widget-body-div">
                        <ServiceBuildsResults result="success" color="#37BC37" builds={this.props.services.length === 0 ? this.state.services.filter((service) => {return service.result === "success"}) : this.props.services.filter((service) => {return service.result === "success"})}/>
                        <ServiceBuildsResults result="failed" color="#B61212" builds={this.props.services.length === 0 ? this.state.services.filter((service) => {return service.result === "failed"}) : this.props.services.filter((service) => {return service.result === "failed"})}/>
                    </div>
                </div>
            </Paper>
        </Grid>
        );
    }
}

export default lastBuildsSummary;
