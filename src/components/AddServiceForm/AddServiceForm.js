import React, { Component } from "react";
import "./AddServiceForm.css";
import Fab from "@material-ui/core/Fab";
import { Add } from "@material-ui/icons";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { handleTextChange, handleSelectChange } from "../../utils/utils";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

class AddServiceForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      addServiceName: "",
      errorText: "",
      validate_addServiceName: false,
      ciName: 0,
      validate_ciName: false,
      scmName: 0,
      validate_scmName: false,

      ciServiceMock: [
        { name: "first ci service", key: "KFC" },
        { name: "second ci service", key: "MCDONALD" },
        { name: "third ci service", key: "BURGERKING" },
      ],

      scmServiceMock: [
        { name: "first scm service", key: "ITALY" },
        { name: "second scm service", key: "RUSSIA" },
        { name: "third ci service", key: "JAPAN" },
      ],
    };
  }

  handleOpen = () => {
    this.setState({
      open: true,
    });
    console.log(this.state.test);
  };

  handleClose = () => {
    this.setState({
      open: false,
      addServiceName: "",
      errorText: "",
      validate_addServiceName: false,
      ciName: 0,
      validate_ciName: false,
      scmName: 0,
      validate_scmName: false,
    });
  };

  render() {
    return (
      <React.Fragment>
        <div style={{display:"flex"}}>
        <Fab className="addServiceButton" onClick={this.handleOpen}>
          <Add />
        </Fab>
        <span className="buttonConnectorLine"></span>          
        </div>


        <Dialog
          contentStyle={{ width: "100%", maxWidth: "none" }}
          dir="rtl"
          onClose={this.handleClose}
          //   actions={actions}
          modal={true}
          open={this.state.open}
          maxWidth="sm"
          fullWidth={true}
        >
          {/* Only actions can close this dialog */}
          <DialogTitle className="dialogTitle">הוספת רכיב</DialogTitle>
          <DialogContent>
            <React.Fragment>
              <TextField
                error={!this.state.validate_addServiceName}
                helperText={this.state.errorText}
                id="addServiceName"
                label="שם רכיב בעברית"
                value={this.state.addServiceName}
                onChange={(event) => handleTextChange(event, this)}
                className="textField"
              />
            </React.Fragment>
            <div style={{display:"flex"}}>
            <div>
              <p className="addServiceFromTitle selectTitle">שם הרכיב בכלי ה-ci</p>
              <Select
                value={this.state.ciName}
                onChange={(event) => handleSelectChange(event, this, "ciName")}
                className="selectField"
              >
                <MenuItem value={0} style={{width: "100%"}}></MenuItem>
                {this.state.ciServiceMock.map((ciServiceMock) => (
                  <MenuItem value={ciServiceMock.name}>
                    {ciServiceMock.name}
                  </MenuItem>
                ))}
              </Select>
              </div>
            <div style={{marginRight:"10%"}}>
              <p className="addServiceFromTitle selectTitle">שם הרכיב בכלי ה-scm</p>
              <Select
                value={this.state.scmName}
                onChange={(event) => handleSelectChange(event, this, "scmName")}
                className="selectField"
              >
                <MenuItem value={0} style={{width: "100%"}}></MenuItem>
                {this.state.scmServiceMock.map((scmServiceMock) => (
                  <MenuItem value={scmServiceMock.name}>
                    {scmServiceMock.name}
                  </MenuItem>
                ))}
              </Select>
              </div>
              </div>
            <Button
              disabled={
                !this.state.validate_addServiceName ||
                !this.state.validate_ciName ||
                !this.state.validate_scmName
              }
              onClick={this.handleClose}
              primary={true}
              className="sendButton"
            >
              הוסף
            </Button>
          </DialogContent>
        </Dialog>
      </React.Fragment>
    );
  }
}

export default AddServiceForm;
