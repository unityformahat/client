import React, { Component } from 'react';
import './ServiceDataRow.css';
import ServiceName from '../ServiceName/ServiceName.js';
import ServiceLastBuild from '../ServiceLastBuild/ServiceLastBuild.js';
import ServiceLastCommit from '../ServiceLastCommit/ServiceLastCommit.js';
import ServiceLastPullRequest from '../ServiceLastPullRequest/ServiceLastPullRequest.js';
import Grid from '@material-ui/core/Grid';


class ServiceDataRow extends Component {
    render() {
        return (
            <div id="serviceDataRow">
                <Grid container id="ServiceRowGridContainer">
                    {/* Opened pull request */}
                    <ServiceLastPullRequest serviceInfo={this.props.serviceInfo} pullRequestsInfo={this.props.pullRequestsInfo}></ServiceLastPullRequest>
                    
                    {/* Last commit to master */}
                    <ServiceLastCommit serviceInfo={this.props.serviceInfo} commitsInfo={this.props.commitsInfo}></ServiceLastCommit>
                    
                    {/* Last build */}
                    <ServiceLastBuild index={this.props.index} buildInfo={this.props.buildInfo} ></ServiceLastBuild>
                    
                    {/* Service Name */}
                    <ServiceName buildInfo={this.props.buildInfo}></ServiceName>
                </Grid>
            </div>
        );
    }
}

export default ServiceDataRow;
