import React, { Component } from 'react';
import './ServiceLastPullRequest.css';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';


class ServiceLastPullRequest extends Component {
    constructor(props) {
        super(props)
        this.state = {
            pullRequestsTitleText: "בקשות מיזוג פתוחות"
        }
    }
    render() {
        return (
            <React.Fragment>
                {/* Service Last Commit */}
                {/* A better implementation would've created a service card header component! */}
                <Grid container item xs={3} sm={3} style={{maxHeight:"100%"}}>
                    <Grid item xs={11} sm={11}>
                    <Card className="servicePaper">
                        <CardActionArea id="action" onClick={event => window.open("http://52.169.187.138:8085/browse/" + this.props.serviceInfo.bambooPlanKey)}>
                            <CardContent id="content">
                                <Grid container spacing={16} xs={16} direction="row" className="height">
                                    <Grid item container xs={15} sm direction="column" className="height">
                                        <Grid item container xs={3} style={{ maxWidth: "100%", justifyContent: "space-between" }}>
                                            <Grid item xs={3} id="dateGrid" className="height" style={{ maxWidth: "100%" }}>
                                                <div id="lastPullRequestdate">{new Date(this.props.pullRequestsInfo.open_pull_requests_to_master[0].create_date).toLocaleDateString('en-GB')}</div>

                                            </Grid>
                                            <Grid item xs={8} sm={6} id="userInfoGrid">
                                                <div id="pullRequestsTitle">
                                                    <div>{this.props.pullRequestsInfo.open_pull_requests_to_master.length}</div>&nbsp;
                                                    <div>{this.state.pullRequestsTitleText}</div>
                                                </div>
                                            </Grid>
                                            {/*  */}
                                        </Grid>
                                        <Divider variant="middle" style={{ backgroundColor: "#314063" }} />

                                        <Grid item container direction="row" xs={7} className="black-scroll-bar" style={{overflowX:"hidden", overflowY: "auto", maxWidth: "100%" }}>
                                            {this.props.pullRequestsInfo.open_pull_requests_to_master.map((pullRequest) =>
                                                <Grid item container direction="row-reverse" className="pullRequestDataContainer">
                                                    <Grid item xs={4} className="userInfoGrid">
                                                        <div className="pullRequestCreator">{pullRequest.developername}</div>
                                                        <AccountCircleOutlinedIcon className="lastPullRequestUserIcon"></AccountCircleOutlinedIcon>
                                                    </Grid>
                                                    <Grid item xs={8} className="pullRequestSourceBranch">
                                                        <div className="sourceBranchDiv">{pullRequest.frombranchname}</div>
                                                    </Grid>
                                                </Grid>
                                            )
                                            }
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={1} id="buildResult" style={{ backgroundColor: "#314063" }}>
                                    </Grid >

                                </Grid>
                            </CardContent>
                        </CardActionArea>
                    </Card>

                    </Grid>
                    
                    <Grid item xs={1} sm={1} id="lastCommitConnectorContainer" className="connectorContainer">
                        <span className="developmentComponentsConnector"></span>
                    </Grid>
                </Grid>
            </React.Fragment>
        );
    }
}

export default ServiceLastPullRequest;
