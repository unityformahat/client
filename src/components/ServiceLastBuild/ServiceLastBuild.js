import React, { Component } from 'react';
import './ServiceLastBuild.css';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import '../BuildCard/BuildCard.css';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';
import Typography from '@material-ui/core/Typography';


class ServiceLastBuild extends Component {
    constructor(props) {
        super(props)
        this.state = {
            BuildersNames: ["איתי עטס", "רונן אנוקא", "יובל רון", "גיא שר", "הדס משה", "אלון כהנא"]
        }
    }
    buildColor = (result) => {
        if (result === true) {
            return "#9ACB62";
        }
        else if (result === false) {
            return "#C25149";
        }
        else {
            return "#707070";
        }
    }

    getBuilderName(buildIndex) {
        return (buildIndex % 5)
    }

    render() {
        return (
            <React.Fragment>
                {/* Service Last Build */}
                {/* A better implementation would've created a service card header component! */}
                <Grid container item xs={4} sm={4}>
                    <Grid item xs={1} sm={1} id="lastbuildConnectorContainer" className="connectorContainer">
                        <span className="developmentComponentsConnector"></span>
                    </Grid>
                    <Grid item xs={11} sm={11}>
                        <Card className="servicePaper">
                            <CardActionArea id="buildCardActionArea" onClick={event => window.open("http://52.169.187.138:8085/browse/" + this.props.buildInfo.plankey)}>
                                <CardContent id="content">
                                    <Grid container spacing={16} xs={16} direction="row" className="height">
                                        <Grid item container xs={15} sm direction="column" className="height">
                                            <Grid item container xs={3} direction="row" className="height" style={{ maxWidth: "100%", justifyContent: "space-between" }}>
                                                <Grid item xs={3} id="dateGrid" className="height" style={{ maxWidth: "100%" }}>
                                                    <div id="date">{this.props.buildInfo.buildsresults[0] ? new Date(this.props.buildInfo.buildsresults[0].buildstarttime).toLocaleDateString('en-GB') : ""}</div>
                                                </Grid>
                                                <Grid item xs={8} sm={6} id="userInfoGrid" className="height">
                                                    {this.props.buildInfo.buildsresults[0] ? <div id="name" style={{ color: this.buildColor(this.props.buildInfo.buildsresults[0].buildstate) }}>{this.props.buildInfo.buildsresults[0].buildername}</div> :
                                                        <div id="name" style={{ color: "#707070" }}>{this.props.buildInfo.buildsresults[0].buildername}</div>}
                                                    {this.props.buildInfo.buildsresults[0] ? <AccountCircleOutlinedIcon id="userIcon" style={{ color: this.buildColor(this.props.buildInfo.buildsresults[0].buildstate) }}></AccountCircleOutlinedIcon> :
                                                        <AccountCircleOutlinedIcon id="userIcon" style={{ color: "#707070" }}></AccountCircleOutlinedIcon>}
                                                </Grid>
                                            </Grid>
                                            {this.props.buildInfo.buildsresults[0] ? <Divider variant="middle" style={{ backgroundColor: this.buildColor(this.props.buildInfo.buildsresults[0].buildstate) }} /> :
                                                <Divider variant="middle" style={{ backgroundColor: "#707070" }} />}

                                            <Grid item container xs={8} direction="row" className="height" style={{ maxWidth: "100%" }}>
                                                <Grid item container xs={6} direction="column" className="leftDataContainer" style={{ maxWidth: "100%" }}>

                                                    <Grid item xs={9} id="lastBuildResultsTitle">
                                                        <Typography id="latestResultsLabel" variant="h5" color="textSecondary" style={{ fontFamily: "Calibri" }}>
                                                            :תוצאות אחרונות
                                                    </Typography>
                                                    </Grid>
                                                    <Grid item xs={3} id="previousBuilds">
                                                        {this.props.buildInfo.buildsresults.slice(1, 8).map((result) => (
                                                            <div id="previous" style={{ backgroundColor: this.buildColor(result.buildstate) }}></div>
                                                        ))}
                                                    </Grid>
                                                </Grid>
                                                <Grid item container xs={6} direction="row" id="rightDataContainer">
                                                    <Grid item xs={5} id="unitTestsNumber">
                                                        <Typography id="unitTestsNumberLabel" variant="h5" color="textSecondary" style={{ fontFamily: "Calibri" }}>
                                                            {this.props.buildInfo.buildsresults[0] ? this.props.buildInfo.buildsresults[0].faildtestcount + this.props.buildInfo.buildsresults[0].successfultestcount : ""}
                                                        </Typography>
                                                    </Grid>
                                                    <Grid item xs={5} id="testingTitle" className="height">
                                                        <Typography id="testingLabel" variant="h5" color="textSecondary" style={{ fontFamily: "Calibri" }}>
                                                            :בדיקות יחידה
                                                    </Typography>
                                                    </Grid>
                                                </Grid>

                                            </Grid>

                                        </Grid>
                                        {this.props.buildInfo.buildsresults[0] ? <Grid item xs={1} id="buildResult" style={{ backgroundColor: this.buildColor(this.props.buildInfo.buildsresults[0].buildstate) }}>
                                        </Grid > :
                                            <Grid item xs={1} id="buildResult" style={{ backgroundColor: "#707070" }}>
                                            </Grid >}
                                    </Grid>

                                </CardContent>
                            </CardActionArea>
                        </Card>
                        <Divider variant="middle" />

                    </Grid>
                </Grid >
            </React.Fragment >
        );
    }
}

export default ServiceLastBuild;
