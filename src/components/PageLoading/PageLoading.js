import React, { Component } from 'react';
import './PageLoading.css';

class PageLoading extends Component {
    render() {
        return (
            <div id={this.props.page} class="loader">
                <div class="inner one"></div>
                <div class="inner two"></div>
                <div class="inner three"></div>
            </div>

        );
    }
}

export default PageLoading;
