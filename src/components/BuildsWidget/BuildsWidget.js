import React, { Component } from 'react';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import ListSubheader from '@material-ui/core/ListSubheader';
import BuildCard from '../BuildCard/BuildCard.js';
import './BuildsWidget.css';
import { withRouter } from 'react-router-dom';
import {connect} from 'react-redux';

class BuildsWidget extends Component {
    constructor(props) {
        super(props)
        this.state = {
            buildsInfo: props.buildsInfo,
        }
    }


    render() {console.log(this.props)
        return (
                <div className="BuildsContainer" >
                    <GridList cellHeight={160} className="gridList" spacing={0}>
                        <GridListTile key="Subheader" cols={1} style={{ height: 'auto', display: 'flex', justifyContent: 'flex-end', width: '100%' }}>
                            <ListSubheader component="div" style={{ fontFamily: "Calibri", fontSize: "120%" }}>תוצאות הבילדים האחרונים</ListSubheader>
                        </GridListTile >
                        {this.props.buildsInfo && Array.isArray(this.props.buildsInfo.plans) ? this.props.buildsInfo.plans.map(build => (
                            <GridListTile key={build.plankey} cols={2} >
                                <div className="buildCell">
                                    <BuildCard key={build.plankey} buildInfo={build}>
                                    \   </BuildCard>
                                </div>
                            </GridListTile>
                        )) : 0}
                    </GridList>
                </div>
        );
    }
}

const mapStateToProps = (state, ownProps)=>{
    return {
        buildsInfo: state.ChangeDashboard.data,
    }
}

export default connect(mapStateToProps)(withRouter(BuildsWidget));