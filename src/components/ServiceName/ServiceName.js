import React, { Component } from 'react';
import './ServiceName.css';
import Grid from '@material-ui/core/Grid';
import { Paper } from '@material-ui/core';


class ServiceName extends Component {
    render() {
        return (
            <React.Fragment>
                {/* Service Name */}
                <Grid container item xs={2} sm={2}>
                    <Grid item xs={2} sm={2} className="connectorContainer">
                        <span className="developmentComponentsConnector"></span>
                    </Grid>
                    <Grid item xs={10} sm={10}>
                        <Paper className="servicePaper">
                            <div className="serviceDiv" id="serviceName">
                                <p style={{ marginLeft: "auto", marginRight: "auto" }}>{this.props.buildInfo.planname}
                                </p>
                            </div>
                        </Paper>
                    </Grid>




                </Grid>

            </React.Fragment>
        );
    }
}

export default ServiceName;
