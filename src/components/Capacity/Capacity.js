import React, { Component } from "react";
import "./Capacity.css";

class Capacity extends Component {
  constructor(props) {
    super(props)
    this.state = {

    }
  }
  FillBeer() {
    var beerHeight = this.props.capacityPercentage / 100 * 340;
    document.getElementById("beerContent").style.height = "" + beerHeight + "px";
  }

  componentDidMount() {
    this.FillBeer();
  }
  render() {
    return (
      <div>
        <div class="beer-glass">
          <div class="glass">
            <div class="bump"></div>
            <div class="bump"></div>
            <div class="bump"></div>
          </div>
          <div id="beerContent" class="beer">
            <div class="foam"></div>
            <div class="foam"></div>
            <div class="foam"></div>
            <div class="foam"></div>
            <div class="foam"></div>
          </div>
        </div>
      </div>
    );
  }
}

export default Capacity;
