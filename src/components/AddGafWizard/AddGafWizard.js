import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import Button from "@material-ui/core/Button";
import Fab from "@material-ui/core/Fab";
import { Add } from "@material-ui/icons";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import TextField from "@material-ui/core/TextField";
import "./AddGafWizard.css";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import DialogTitle from "@material-ui/core/DialogTitle";
import { StepLabel } from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import MobileStepper from "@material-ui/core/MobileStepper";
import {handleTextChange, handleSelectChange} from '../../utils/utils'
export default class AddGafWizard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // this data init for default values for the component to use
      openFirst: false,
      openSecond: false,
      gafName: "",
      validate_gafName: false,
      unityName: "",
      validate_unityName: false,
      unitName: 0,
      validate_unitName: false,
      ciName: 0,
      validate_ciName: false,
      scmName: 0,
      validate_scmName: false,
      jiraName: 0,
      validate_jiraName: false,

      errorText: "",
      stepIndex: 0,
      NevigateStepperIndex: 0,

      // this data should come from redux
      ciProjectMock: [
        { name: "first ci project", key: "HAHA" },
        { name: "second ci project", key: "NANA" },
        { name: "third ci project", key: "PAPA" },
      ],

      scmProjectMock: [
        { name: "first scm project", key: "DOTA" },
        { name: "second scm project", key: "LOL" },
        { name: "third ci project", key: "PARAGON" },
      ],

      jiraProjectMock: [
        { name: "first jira project", key: "CSGO" },
        { name: "second jira project", key: "COD" },
        { name: "third jira project", key: "GTA" },
      ],
    };
  }

  handleOpenFirst = () => {
    this.setState({
      openFirst: true,
      openSecond: false,
      NevigateStepperIndex: 0,
    });
  };
  handleOpenSecond = () => {
    this.setState({
      openSecond: true,
      openFirst: false,
      NevigateStepperIndex: 1,
    });
  };

  handleClose = () => {
    this.setState({
      openFirst: false,
      openSecond: false,
      gafName: "",
      validate_gafName: false,
      unityName: "",
      validate_unityName: false,
      unitName: 0,
      validate_unitName: false,
      ciName: 0,
      validate_ciName: false,
      scmName: 0,
      validate_scmName: false,
      jiraName: 0,
      validate_jiraName: false,

      errorText: "",
      stepIndex: 0,
      NevigateStepperIndex: 0,
    });
  };

  handleNext = () => {
    const { stepIndex } = this.state;
    // if we are at the first page
    if (this.state.openFirst) {
      if (stepIndex < 2) {
        this.setState({ stepIndex: stepIndex + 1 });
      }
      if (stepIndex === 2) {
        this.handleOpenSecond();
        this.setState({ stepIndex: 0 });
      }
    }
    //if we are at the second page
    if (this.state.openSecond) {
      if (stepIndex < 3) {
        this.setState({ stepIndex: stepIndex + 1 });
      }
    }
  };

  handlePrev = () => {
    const { stepIndex } = this.state;
    //if we are at the first page
    if (this.state.openFirst) {
      if (stepIndex > 0) {
        this.setState({ stepIndex: stepIndex - 1 });
      }
    }
    //if we are at the second page
    if (this.state.openSecond) {
      if (stepIndex > 0) {
        this.setState({ stepIndex: stepIndex - 1 });
      }
      if (stepIndex === 0) {
        this.handleOpenFirst();
        this.setState({ stepIndex: 2 });
      }
    }
  };
  
  getStepContentFirst(stepIndex) {
    switch (stepIndex) {
      case 0:
        return (
          <React.Fragment>
            <Select
              style={{ right: "3%" }}
              value={this.state.unitName}
              onChange={event => handleSelectChange(event, this, "unitName")}
              // id="unitName"
            >
              <MenuItem value={0}>בחר יחידה</MenuItem>
              <MenuItem value={"נשרי"}>נשרי</MenuItem>
              <MenuItem value={"רוני"}>רוני</MenuItem>
              <MenuItem value={"גלוז"}>גלוז</MenuItem>
              <MenuItem value={"סופי"}>סופי</MenuItem>
              <MenuItem value={"קובי"}>קובי</MenuItem>
            </Select>
          </React.Fragment>
        );
      case 1:
        return (
          <React.Fragment>
            <TextField
              error={!this.state.validate_gafName}
              helperText={this.state.errorText}
              style={{ right: "3%" }}
              id="gafName"
              label="שם גף בעברית"
              value={this.state.gafName}
              onChange={event => handleTextChange(event, this)}
            />
          </React.Fragment>
        );
      case 2:
        return (
          <React.Fragment>
            <TextField
              error={!this.state.validate_unityName}
              helperText={this.state.errorText}
              style={{ right: "3%" }}
              id="unityName"
              label="שם הפרוייקט בעברית"
              value={this.state.unityName}
              onChange={event => handleTextChange(event, this)}
            />
          </React.Fragment>
        );

      default:
        console.log("Step Index is null for some reasson");
    }
  }

  getStepContentSecond(stepIndex) {
    switch (stepIndex) {
      case 0:
        return (
          <React.Fragment>
            <Select
              style={{ right: "3%" }}
              value={this.state.ciName}
              onChange={event => handleSelectChange(event, this, "ciName")}
            >
              <MenuItem value={0}>בחר פרוייקט בכלי ה-ci</MenuItem>
              {this.state.ciProjectMock.map((ciProjectMock) => (
                <MenuItem value={ciProjectMock.name}>
                  {ciProjectMock.name}
                </MenuItem>
              ))}
            </Select>
          </React.Fragment>
        );

      case 1:
        return (
          <React.Fragment>
            <Select
              style={{ right: "3%" }}
              value={this.state.scmName}
              onChange={event => handleSelectChange(event, this, "scmName")}
            >
              <MenuItem value={0}>בחר פרוייקט בכלי ה-scm</MenuItem>
              {this.state.scmProjectMock.map((scmProjectMock) => (
                <MenuItem value={scmProjectMock.name}>
                  {scmProjectMock.name}
                </MenuItem>
              ))}
            </Select>
          </React.Fragment>
        );

      case 2:
        return (
          <React.Fragment>
            <Select
              style={{ right: "3%" }}
              value={this.state.jiraName}
              onChange={event => handleSelectChange(event, this, "jiraName")}
            >
              <MenuItem value={0}>שם הפרויקט ב-Jira</MenuItem>
              {this.state.jiraProjectMock.map((jiraProjectMock) => (
                <MenuItem value={jiraProjectMock.name}>
                  {jiraProjectMock.name}
                </MenuItem>
              ))}
            </Select>
          </React.Fragment>
        );

      case 3:
        return (
          <React.Fragment>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>שם היחידה</TableCell>
                  <TableCell>שם הגף</TableCell>
                  <TableCell>שם הפרוייקט</TableCell>
                  <TableCell>שם הפרויקט בכלי ה-ci</TableCell>
                  <TableCell>שם הפרויקט בכלי ה-scm</TableCell>
                  <TableCell>שם הפרויקט ב-Jira</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow key={1}>
                  <TableCell
                    style={{ color: this.state.validate_unitName ? "" : "red" }}
                  >
                    {this.state.unitName}
                  </TableCell>
                  <TableCell
                    style={{ color: this.state.validate_gafName ? "" : "red" }}
                  >
                    {this.state.gafName}
                  </TableCell>
                  <TableCell
                    style={{
                      color: this.state.validate_unityName ? "" : "red",
                    }}
                  >
                    {this.state.unityName}
                  </TableCell>
                  <TableCell
                    style={{ color: this.state.validate_ciName ? "" : "red" }}
                  >
                    {this.state.ciName}
                  </TableCell>
                  <TableCell
                    style={{ color: this.state.validate_scmName ? "" : "red" }}
                  >
                    {this.state.scmName}
                  </TableCell>
                  <TableCell
                    style={{ color: this.state.validate_jiraName ? "" : "red" }}
                  >
                    {this.state.jiraName}
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </React.Fragment>
        );

      default:
        console.log("Step Index is null for some reasson");
    }
  }

  generateFirstStepper() {
    const { stepIndex } = this.state;
    return (
      <div>
        <Stepper nonLinear activeStep={stepIndex}>
          <Step>
            <StepLabel onClick={() => this.setState({ stepIndex: 0 })}>
              שם היחידה
            </StepLabel>
          </Step>
          <Step>
            <StepLabel onClick={() => this.setState({ stepIndex: 1 })}>
              שם הגף
            </StepLabel>
          </Step>
          <Step>
            <StepLabel onClick={() => this.setState({ stepIndex: 2 })}>
              שם הפרוייקט
            </StepLabel>
          </Step>
        </Stepper>

        <div className="input-style">
          <div style={{ height: "20%" }}>
            <div style={{ minHeight: "15vh" }}>
              {this.getStepContentFirst(stepIndex)}
            </div>
            <div id="buttonsContainer">
              <Button
                // disabled={stepIndex === 3}
                onClick={this.handleNext}
                style={{ marginRight: 12 }}
              >
                הבא
              </Button>
              <Button
                disabled={stepIndex === 0}
                onClick={this.handlePrev}
                style={{ marginRight: 12 }}
              >
                הקודם
              </Button>
              <div className="FirstPageNevigateStepper">
              {this.generateNevigateStepper()}  
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  generateSecondStepper() {
    const { stepIndex } = this.state;
    return (
      <div>
        <Stepper nonLinear activeStep={stepIndex}>
          <Step>
            <StepLabel onClick={() => this.setState({ stepIndex: 0 })}>
              שם הפרויקט בכלי ה-ci
            </StepLabel>
          </Step>
          <Step>
            <StepLabel onClick={() => this.setState({ stepIndex: 1 })}>
              שם הפרויקט בכלי ה-scm
            </StepLabel>
          </Step>
          <Step>
            <StepLabel onClick={() => this.setState({ stepIndex: 2 })}>
              שם הפרויקט ב-Jira
            </StepLabel>
          </Step>
          <Step>
            <StepLabel onClick={() => this.setState({ stepIndex: 3 })}>
              סיכום
            </StepLabel>
          </Step>
        </Stepper>

        <div className="input-style">
          <div style={{ height: "20%" }}>
            <div style={{ minHeight: "15vh" }}>
              {this.getStepContentSecond(stepIndex)}
            </div>
            <div id="buttonsContainer">
              <Button
                disabled={
                  stepIndex !== 3 ||
                  !this.state.validate_gafName ||
                  !this.state.validate_unityName ||
                  !this.state.validate_unitName ||
                  !this.state.validate_ciName ||
                  !this.state.validate_scmName ||
                  !this.state.validate_jiraName
                }
                onClick={this.handleClose}
                primary={true}
              >
                סיום
              </Button>
              <Button
                disabled={stepIndex === 3}
                onClick={this.handleNext}
                style={{ marginRight: 12 }}
              >
                הבא
              </Button>
              <Button onClick={this.handlePrev} style={{ marginRight: 12 }}>
                הקודם
              </Button>
              <div className="SecondPageNevigateStepper">
              {this.generateNevigateStepper()}  
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  generateNevigateStepper() {
    const { NevigateStepperIndex } = this.state;

    return (
      <MobileStepper
        // className="NevigateStepper"
        activeStep={NevigateStepperIndex}
        variant="dots"
        steps={2}
        position="static"
      >
      </MobileStepper>
    );
  }

  render() {
    const actions = [
      <Button label="cancel" primary={true} onClick={this.handleClose} />,
    ];
    return (
      <React.Fragment>
        <Fab className="add-service-btn" onClick={this.handleOpenFirst}>
          <Add />
        </Fab>

        {/* first part */}
        <Dialog
          contentStyle={{ width: "100%", maxWidth: "none" }}
          dir="rtl"
          onClose={this.handleClose}
          actions={actions}
          modal={true}
          open={this.state.openFirst}
          className="wizard"
          maxWidth="lg"
          fullWidth={true}
        >
          {/* Only actions can close this dialog */}
          <DialogTitle id="simple-dialog-title">
            הוספת פרוייקט ל-Unity
          </DialogTitle>
          <DialogContent>
            <p>חלק א'- נתונים כלליים</p>
            {this.generateFirstStepper()}
          </DialogContent>
        </Dialog>

        {/* second part */}
        <Dialog
          contentStyle={{ width: "100%", maxWidth: "none" }}
          dir="rtl"
          onClose={this.handleClose}
          actions={actions}
          modal={true}
          open={this.state.openSecond}
          className="wizard"
          maxWidth="lg"
          fullWidth={true}
        >
          {/* Only actions can close this dialog */}
          <DialogTitle id="simple-dialog-title">
            הוספת פרוייקט ל-Unity
          </DialogTitle>
          <DialogContent>
            <p>חלק ב'- חיבור לכלים</p>
            {this.generateSecondStepper()}
          </DialogContent>
        </Dialog>
      </React.Fragment>
    );
  }
}
