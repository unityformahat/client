import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import { createMuiTheme } from '@material-ui/core'
import Drawer from '@material-ui/core/Drawer';
import ProjectsMenu from '../ProjectMenu/ProjectsMenu'
import AddGafWizard from '../AddGafWizard/AddGafWizard'
import { Toolbar, Typography } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu';
import { Link as RouterLink } from 'react-router-dom'
import './SideBar.css';


const theme = createMuiTheme({

    typography: {
        fontFamily: "Calibri",
    },
    palette: {
        primary: {
            main: '#44AEFF'
        },


    },
    direction: 'rtl',
});

export default class SideBar extends Component {
    constructor(props) {
        super(props)

        let currentDate = new Date()

        this.state = {

            open: false, addServiceOpen: false, time:
            {
                year: currentDate.getFullYear(), month: currentDate.getMonth(), dayM: currentDate.getDate(),
                dayW: currentDate.getDay(), hours: currentDate.getHours(), minutes: currentDate.getMinutes()
            }
        }
    }

    indexToDate = {
        0: "ראשון",
        1: "שני",
        2: "שלישי",
        3: "רביעי",
        4: "חמישי",
        5: "שישי",
        6: "שבת"

    }

    handleToggle = () => this.setState({ open: !this.state.open })

    handleFloatingActionButton = () => {
        this.setState({ addServiceOpen: !this.state.addServiceOpen })
    }

    componentDidMount() {
        this.intervalID = setInterval(() => this.tick(), 5000)
    }

    componentWillUnmount() {
        clearInterval(this.intervalID)
    }

    tick() {
        let d = new Date()
        this.setState({ time: { year: d.getFullYear(), month: d.getMonth(), dayM: d.getDate(), dayW: d.getDay(), hours: d.getHours(), minutes: d.getMinutes() } })
    }

    render() {

        var datetime =
            "יום " + this.indexToDate[this.state.time.dayW] + " ," +
            (this.state.time.dayM < 10 ? '0' : '') + this.state.time.dayM + "/" +
            (this.state.time.month + 1 < 10 ? '0' : '') + (this.state.time.month + 1) + "/"
            + this.state.time.year + " , "
            + (this.state.time.hours < 10 ? '0' : '') + this.state.time.hours + ":"
            + (this.state.time.minutes < 10 ? '0' : '') + this.state.time.minutes

        return (

            <MuiThemeProvider theme={theme}>
                <AppBar id="appBar" style={{ zIndex: "1", boxShadow: "none"}}>

                    <div style={{ fontFamily: "Calibri", fontSize: "120%"}} dir="rtl">



                        <Toolbar>
                            {/* menu icon button */}
                            <IconButton id="hamburgerIcon" aria-label="Menu" onClick={this.handleToggle} style={{color:"white"}} >
                                <MenuIcon />
                            </IconButton>
                            

                            <RouterLink to={'/'}>
                                <div style={{display: 'flex', flexDirection: 'row'}}>
                                    <img src={require('../../assets/unity_logo2.png')} style={{width: '40px', height:'40px', display: 'flex'}} alt=''/>
                                    <Typography variant="h5" id="unity-title" style={{
                                        display: 'flex',
                                        flex: '1',
                                        color: 'white',
                                        fontWeight: '700',
                                        fontFamily: "Calibri"
                                    }} >Unity</Typography>
                                </div>

                            </RouterLink>

                            <div style={{
                                display: "flex", flexDirection: "column", left: '2%',
                                position: 'absolute'
                            }}>
                                <Typography variant="h6" style={{
                                    color: 'white',
                                    left: '12%',
                                    flexGrow: 1
                                }}>ברוך הבא לוי</Typography>
                                <Typography variant="subtitle1" id="date-and-time" style={{
                                    color: 'white',
                                    left: '1%',
                                    flexGrow: 1,
                                    flex: 1
                                }}>{datetime}</Typography>
                            </div>

                        </Toolbar>  </div>
                </AppBar>


                <Drawer
                    anchor="left"
                    width='auto'
                    open={this.state.open}
                    onClose={this.handleToggle}>
                    <ProjectsMenu closeCallback={this.handleToggle}/>
                    <AddGafWizard />
                </Drawer>
            </MuiThemeProvider>

        );
    }
}