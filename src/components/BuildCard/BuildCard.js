import React, { Component } from 'react';
import './BuildCard.css';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

class BuildCard extends Component {
    buildColor = (result) => {
        if (result === true) {
            return "#9ACB62";
        } else {
            return "#C25149";
        }
    }

    render() {
        return (
            <div id="BuildCard">
                <Card id="BuildPaper">
                    <CardActionArea id="action" onClick={event => window.open("https://bamboo.app.iaf/browse/" + this.props.buildInfo.planKey)}>
                        <CardContent id="content">
                            <Grid container spacing={16} xs={16} direction="row" className="height">
                                <Grid item container xs={12} sm direction="column" className="height">
                                    <Grid item container xs={12} sm direction="row" className="height">
                                        {/* Top left Container */}
                                        <Grid item xs={6} className="height item">
                                            <Typography variant="h5" component="h2" align="left" style={{ fontWeight: "bold", fontFamily: "Nirmala-ui", color: "#707070" }}>
                                                {this.props.buildInfo.plankey}
                                            </Typography>
                                        </Grid>
                                        <Grid item container xs={6} sm direction="column" id="lastBuildDetailsContainer" className="height">
                                            <Grid item xs={6} id="buildDate" className="height">
                                                <Typography variant="h5" align="right" color="textSecondary" style={{ fontFamily: "Nirmala-ui" }}>
                                                    {this.props.buildInfo.buildsresults[0] ? new Date(this.props.buildInfo.buildsresults[0].buildstarttime).toLocaleDateString('en-GB') : console.log("no build info")}
                                                </Typography>
                                            </Grid>
                                            <Grid item xs={6} id="builderName" className="height">
                                                <Typography variant="h6" align="right" color="textSecondary" style={{ fontFamily: "Calibri" }}>
                                                    {this.props.buildInfo.buildsresults[0] ? this.props.buildInfo.buildsresults[0].buildNumber : console.log("no build info")}
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                    </Grid>

                                    {/* Bottom Container */}
                                    <Grid container xs={12} sm direction="row" className="height">
                                        <Grid item xs={5} id="previousBuilds">
                                            {this.props.buildInfo.buildsresults.slice(0,7).map((result) => (
                                                <div id="previous" style={{ backgroundColor: this.buildColor(result.buildstate) }}></div>
                                            ))}
                                        </Grid>
                                        <Grid item xs={3} id="resultsTitle">
                                            <Typography id="latestResultsLabel" variant="h5" color="textSecondary" style={{ fontFamily: "Calibri" }}>
                                                :תוצאות אחרונות
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={2} id="unitTestsNumber">
                                            <Typography id="unitTestsNumberLabel" variant="h5" color="textSecondary" style={{ fontFamily: "Calibri" }}>
                                                {this.props.buildInfo.buildsresults[0] ? this.props.buildInfo.buildsresults[0].faildtestcount + this.props.buildInfo.buildsresults[0].successfultestcount: console.log("no build info")}
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={2} id="testingTitle" className="height">
                                            <Typography id="testingLabel" variant="h5" color="textSecondary" style={{ fontFamily: "Calibri" }}>
                                                :בדיקות יחידה
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Grid item id="buildResult">
                                    <div className="latestResult" style={{ backgroundColor: this.buildColor(this.props.buildInfo.buildsresults[0] ? this.props.buildInfo.buildsresults[0].buildstate : console.log("no build info")) }}></div>
                                </Grid >

                            </Grid>
                        </CardContent>
                    </CardActionArea>
                </Card>
            </div>
        );
    }
}

export default BuildCard;
