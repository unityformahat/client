import React, { Component } from 'react';
import './LastCriticalBugs.css';
import Grid from '@material-ui/core/Grid';
import { Paper } from '@material-ui/core';
import CriticalBug from '../CriticalBug/CriticalBug';
import BugReportIcon from '@material-ui/icons/BugReport';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';


class lastCriticalBugs extends Component {
    constructor(props) {
        super(props)
        this.state = {
            criticalBugs: [
                {
                    bug_id: 1,
                    bug_name: "דף בית",
                    details: "שרת קורס בכפתור הוספה"
                },
                {
                    bug_id: 2,
                    bug_name: "דף טיל",
                    details: "אין הרשאת כניסה למנהל"
                },
                {
                    bug_id: 3,
                    bug_name: "דף בית",
                    details: "שרת קורס בכפתור הוספה"
                },
                {
                    bug_id: 4,
                    bug_name: "דף טיל",
                    details: "אין הרשאת כניסה למנהל"
                },
                {
                    bug_id: 5,
                    bug_name: "דף בית",
                    details: "שרת קורס בכפתור הוספה"
                },
                {
                    bug_id: 6,
                    bug_name: "דף טיל",
                    details: "אין הרשאת כניסה למנהל"
                },
                {
                    bug_id: 7,
                    bug_name: "דף בית",
                    details: "שרת קורס בכפתור הוספה"
                },
                {
                    bug_id: 8,
                    bug_name: "דף טיל",
                    details: "אין הרשאת כניסה למנהל"
                },
                {
                    bug_id: 9,
                    bug_name: "דף בית",
                    details: "שרת קורס בכפתור הוספה"
                },
                
                {
                    bug_id: 10,
                    bug_name: "דף טיל",
                    details: "אין הרשאת כניסה למנהל"
                },
                {
                    bug_id: 11,
                    bug_name: "דף בית",
                    details: "שרת קורס בכפתור הוספה"
                },
                
                {
                    bug_id: 12,
                    bug_name: "דף טיל",
                    details: "אין הרשאת כניסה למנהל"
                },
                {
                    bug_id: 13,
                    bug_name: "דף בית",
                    details: "שרת קורס בכפתור הוספה"
                },
            ]
        }
    }
    render() {
        return (
            <Grid item id="bugsSummaryWidget" xs={12} md={6} lg={3} className="summaryWidget">
                <Paper className="widgetPaper">
                    <div className="widgetDiv flexed-div">
                        <div class="summaryIconDiv">
                            <BugReportIcon id="warningIcon" fontSize="large" />
                        </div>
                        <div>
                            <p className="widgetTitle summary-title-padding"> {this.props.criticalBugs.length > 0 ? this.props.criticalBugs.length : this.state.criticalBugs.length} באגים קריטיים </p>
                        </div>
                        <div className="widget-body-div">
                            <GridList cellHeight={100} className="bugsGridList black-scroll-bar" spacing={9} >
                                {
                                    this.props.criticalBugs.length > 0 && Array.isArray(this.state.criticalBugs) ? 
                                    this.props.criticalBugs.map(bug => (
                                        <GridListTile className="bugTile" key={bug.bug_id} cols={2} >
                                            <div className="bugCell">
                                                <CriticalBug bug={bug}></CriticalBug>
                                            </div>
                                        </GridListTile>
                                    ))
                                    :                                
                                this.state.criticalBugs && Array.isArray(this.state.criticalBugs) ? this.state.criticalBugs.map(bug => (
                                    <GridListTile className="bugTile" key={bug.bug_id} cols={2} >
                                        <div className="bugCell">
                                            <CriticalBug bug={bug}></CriticalBug>
                                        </div>
                                    </GridListTile>
                                )) : 0
                                
                                
                                }
                            </GridList>
                        </div>
                    </div>
                </Paper>
            </Grid>
        );
    }
}

export default lastCriticalBugs;
