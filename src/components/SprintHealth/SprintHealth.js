import React from "react";
import { Chart } from "chart.js";
import "./SprintHealth.css";

class SprintHealth extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // data that should arrive from redux
      Tasks: 50,
      finishedTasks: 28,
      TimeOfSprint: 14,
      TimeOfSprintRemaning: 7,
      TimePassed: 7,

      // component default data
      sprintHealthStatusColor: "",
      sprintHealthStatusSide: "",
    };
  }

  availableHours = () => {
    return Math.abs(this.state.Tasks - this.state.finishedTasks);
  };

  fillCharts = () => {
    const CHART = document.getElementById("pieChart");
    new Chart(CHART, {
      type: "doughnut",
      data: {
        datasets: [
          {
            data: [20, 20, 20, 20, 20],
            backgroundColor: [
              "#FB3636",
              "#FDF26A",
              "#71E771",
              "#FDF26A",
              "#FB3636",
            ],
            hoverBackgroundColor: ["red", "yellow", "green", "yellow", "red"],
          },
        ],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        rotation: Math.PI,
        circumference: Math.PI,
        elements: {
          arc: {
            borderWidth: 0,
          },
        },
        tooltips: {
          enabled: false,
        },
      },
    });
  };

  whatArrow() {
    /**
     * @param sprintHealth = The ratio of the sprint health
     * @param MINIMUM_VALUE = the minimum value of the ratio (0.7)
     * @param MAXIMUM_VALUE = the maximum value of the ratio (1.3)
     * @param range = the current ratio between minimum to maximum
     * @param rangePercent = ratio represent in pecents (between 0-180 degrees)
     */
    const taskPercent = (this.state.finishedTasks / this.state.Tasks) * 100;
    const timePercent =
      (this.state.TimeOfSprintRemaning / this.state.TimeOfSprint) * 100;
    const sprintHealth = timePercent / taskPercent;
    const MINIMUM_VALUE = 0.7; //lowset grade
    const MAXIMUM_VALUE = 1.3; //highest grade
    const range = sprintHealth - MINIMUM_VALUE;
    const rangePercent = (range * 100) / (MAXIMUM_VALUE - MINIMUM_VALUE);

    var degrees;
    if (sprintHealth < MINIMUM_VALUE) {
      degrees = -72;
    } else if (sprintHealth === MINIMUM_VALUE) {
      degrees = -54;
    } else if (sprintHealth > MAXIMUM_VALUE) {
      degrees = 72;
    } else if (sprintHealth === MAXIMUM_VALUE) {
      degrees = 54;
    } else {
      degrees = (rangePercent * 180) / 100 - 90;
    }
    console.log(degrees);
    document.getElementById("needle").style.transform = `rotate(${degrees}deg)`;

    if (degrees >= -54 && degrees <= -18) {
      this.setState({
        sprintHealthStatusColor: "#FDF26A",
        sprintHealthStatusSide: "יש לך חוסר של ",
      });
    }
    if (degrees <= 54 && degrees >= 18) {
      this.setState({
        sprintHealthStatusColor: "#FDF26A",
        sprintHealthStatusSide: "יש לך עודף של ",
      });
    }

    if (degrees >= -90 && degrees < -54) {
      this.setState({
        sprintHealthStatusColor: "#FB3636",
        sprintHealthStatusSide: "יש לך חוסר של ",
      });
    }
    if (degrees <= 90 && degrees > 54) {
      this.setState({
        sprintHealthStatusColor: "#FB3636",
        sprintHealthStatusSide: "יש לך עודף של ",
      });
    }
  }

  componentDidMount() {
    this.fillCharts();
    this.whatArrow();
  }

  render() {
    return (
      <div id="SprintHealthComponent">
        <div style={{ width: "67%" }}>
          <div
            style={{
              width: "100%",
              maxWidth: "100",
              position: "relative",
              display: "flex",
              flexDirection: "column",
            }}
          >
            <div id="chartDiv" style={{ flex: "0", position: "relative" }}>
              <canvas height="200" id="pieChart" />
              <span id="needle"></span>
            </div>
            <div id="overbook">OVERBOOK של שעות</div>
            <div id="underbook">UNDERBOOK של שעות</div>
          </div>
          <div id="sprintHealthStatus">
            {this.state.sprintHealthStatusColor === "" ? (
              "מצב שעות פנויות תקין"
            ) : (
              <div>
                {this.state.sprintHealthStatusSide}
                <span id="sprintHealthStatusNumber"
                  style={{
                    backgroundColor: this.state.sprintHealthStatusColor,
                  }}
                >
                  {this.availableHours()}
                </span>
                {" "}
                שעות פנויות
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default SprintHealth;
