import React from 'react';
import { Chart } from 'chart.js';
import Typography from '@material-ui/core/Typography';
import StopIcon from '@material-ui/icons/Stop';
import Grid from '@material-ui/core/Grid';
import './TasksProgressDiagram.css';

class TasksProgressDiagram extends React.Component {
    statuses= ["Done", "In Progress", "To Do"]
    JiraData = {
        features: {
            "To Do": 9,
            "In Progress": 5,
            "Done": 7
        },
        bugs: {
            "To Do": 5,
            "In Progress": 6,
            "Done": 3
        }
    }
       


    fillCharts = (CurrentJiraData) => {
        const CHART = document.getElementById(CurrentJiraData.status);
        var issuesStatusChart = new Chart(CHART, {
            type: "bar",
            data: {
                datasets: [
                    {
                        label: 'features',
                        backgroundColor: ['#2424B7'],
                        hoverBackgroundColor: ['#0C1C51'],
                        data: [CurrentJiraData.features]
                    },
                    {
                        label: 'bugs',
                        backgroundColor: ['#729CE4'],
                        hoverBackgroundColor: ['#3F80DF'],
                        data: [CurrentJiraData.bugs]
                    }
                ]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            suggestedMin: 0,
                        },
                        display: false
                    }],
                    xAxes: [{
                        display: false
                    }]
                },
                legend: {
                    display: false
                },
                tooltips: {
                    callbacks: {
                        title: function () { return "" }
                    }
                }
            }
        })
        window.issuesStatusCharts.push(issuesStatusChart);
    }

    componentDidMount() {
        window.issuesStatusCharts = [];
        var JiraData = [];
        if (this.props.JiraData) {
            this.statuses.map(status => (
                JiraData.push(
                    {
                        features: this.props.JiraData.features[status],
                        bugs: this.props.JiraData.bugs[status],
                        status: status
                    }
                ) 
            ))
        }
        else {
            this.statuses.map(status => (
                JiraData.push({
                    features: this.JiraData.features[status],
                    bugs: this.JiraData.bugs[status],
                    status: status
                })
        ))
        }

        if (JiraData.length > 0){
            JiraData.map(statusData => (this.fillCharts(statusData)))
        }
        
    }

    componentDidUpdate(){
        var JiraData = [];
        if (this.props.JiraData) {
            this.statuses.map(status => (
                JiraData.push(
                    {
                        features: this.props.JiraData.features[status],
                        bugs: this.props.JiraData.bugs[status],
                        status: status
                    }
                ) 
            ))
        }
        else {
            this.statuses.map(status => (
                JiraData.push({
                    features: this.JiraData.features[status],
                    bugs: this.JiraData.bugs[status],
                    status: status
                })
        ))
        }
        var i = 0;
        if (JiraData.length > 0){
            JiraData.map(statusData => {
                window.issuesStatusCharts[i].data.datasets[0].data = [statusData.features];
                window.issuesStatusCharts[i].data.datasets[1].data = [statusData.bugs];
                i+=1;
            }
            )
        }
        window.issuesStatusCharts.map(issuesStatusChart => issuesStatusChart.update())
    }

    render() {
        return (

            <div style={{ display: "flex", flexDirection: "row", flexGrow: 1, height: "100%", maxHeight: "100%" }}>

                <Grid item container xs={12} id="bugsStatusWidgetContainer" direction="column" >
                    <Grid item container direction="row" style={{ alignItems: "center", justifyContent: "space-around", height: "75%", width: "80%", maxWidth: "100%", }} >
                        {
                            this.statuses.map(status => (
                                <Grid item xs={3} key={status}>
                                    <div style={{ display: "flex", flexDirection: "column" }}>
                                        <canvas height="150%" id={status} />
                                    </div>
                                    <div style={{ display: "flex", direction: "ltr"}}>
                                        <div style={{ flex: "1", textAlign: "center" }}>
                                            {this.props.JiraData ? this.props.JiraData.features[status] : this.JiraData.features[status]}
                                        </div>
                                        <div style={{ flex: "1", textAlign: "center" }}>
                                            {this.props.JiraData ? this.props.JiraData.bugs[status] : this.JiraData.bugs[status]}
                                        </div>
                                    </div>
                                    <Grid item xs={12} style={{ textAlign: "center" }}>
                                        <Typography variant="h5" className="statusLabel" >
                                            {status}
                                        </Typography>
                                    </Grid>
                                </Grid>
                            ))
                        }

                        <Grid item xs={1} >
                            <div style={{ marginTop: "10%", marginLeft: "5%", display: "flex", flexDirection: "column" }}>
                                <div style={{ display: "flex", flexDirection: "row" }}>
                                    <StopIcon style={{ fill: "#2424B7" }} />
                                    <Typography>features</Typography>
                                </div>
                                <br />
                                <div style={{ display: "flex", flexDirection: "row" }}>
                                    <StopIcon style={{ fill: "#729CE4" }} />
                                    <Typography>bugs</Typography>
                                </div>
                            </div>
                        </Grid>
                    </Grid>





                </Grid>
            </div>

        )
    }
}
export default TasksProgressDiagram;