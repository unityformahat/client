import React, { Component } from 'react';
import { KeyboardArrowLeft, KeyboardArrowDown } from '@material-ui/icons'
import MenuItem from '@material-ui/core/MenuItem'
import Divider from '@material-ui/core/Divider'
import './ProjectsMenu.css'
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Avatar from '@material-ui/core/Avatar'
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import Settings from '@material-ui/icons/Settings';
import Link from '@material-ui/icons/Link';
import Person from '@material-ui/icons/Person';
import CellWifi from '@material-ui/icons/CellWifi';
import ChatRounded from '@material-ui/icons/ChatRounded'
import { Collapse } from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom';
import { connect } from 'react-redux';


class ProjectsMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isExpanded: []
        }
    }

    handleUnitExpandClick = (clickedUnit) => {
        let isExpanded = this.state.isExpanded;
        isExpanded[clickedUnit.unit_id - 1].isUnitExpandad = !isExpanded[clickedUnit.unit_id - 1].isUnitExpandad;
        this.setState({ isExpanded: isExpanded })
    }
    handleGafExpandClick = (clickedUnit, clickedGaf) => {
        let isExpanded = this.state.isExpanded;
        isExpanded[clickedUnit.unit_id - 1].gafs[clickedGaf.gaf_id - 1] = !isExpanded[clickedUnit.unit_id - 1].gafs[clickedGaf.gaf_id - 1];
        this.setState({ isExpanded: isExpanded })
    }

    async changeGaf(unit, gaf) {
        // const {dispatch} = this.props;
        this.props.closeCallback();
        // await dispatch(selectGaf(unit, gaf));
    }

    componentDidMount = () => {
        this.setUnitsExpanded();
    }

    async setUnitsExpanded() {
        const { units } = this.props.hierarchy
        let unitsExpanded = []
        let i;
        let y;
        for (i = 0; i < units.length; i++) {
            unitsExpanded.push({ isUnitExpandad: false })
            unitsExpanded[i] = { ...unitsExpanded[i], gafs: [] }
            for (y = 0; y < units[i].gafs.length; y++) {
                unitsExpanded[i].gafs.push(false);
            }
        }
        this.setState({ isExpanded: unitsExpanded })
    }




    render() {
        const { units } = this.props.hierarchy
        return (
            <div style={{overflowY:"auto"}}>
                {/* Avatar Section */}
                <Card dir="rtl" >
                    <CardHeader
                        action={
                            <IconButton disabled={true}>
                                <Settings fontSize="large" className="settings" />
                            </IconButton>
                        }
                        title="אלון כהנא"
                        subheader={new Date().toDateString()}
                        avatar={
                            <Avatar className="avatar" aria-label="Recipe" src="/assets/levi.png" >
                                R
                            </Avatar>
                        }
                    />
                </Card>
                
                <Divider></Divider>

                <MenuItem dir="rtl" onClick={() => this.props.closeCallback()}>
                    <ListItemText className="hebrew-text" inset primary="קישורים" />
                    <ListItemIcon>
                        <Link />
                    </ListItemIcon></MenuItem>

                <MenuItem dir="rtl" style={{ backgroundColor: 'white', fontFamily: 'Calibri-light' }} disabled={true} >
                    <ListItemText className="hebrew-text" inset primary="איזור אישי" />
                    <ListItemIcon > <Person />
                    </ListItemIcon>
                </MenuItem>
                <MenuItem dir="rtl" style={{ backgroundColor: 'white', fontFamily: 'Calibri-light' }} disabled={true} >
                    <ListItemText className="hebrew-text" inset primary="ניטור סביבתי" /><ListItemIcon > <CellWifi />
                    </ListItemIcon>

                </MenuItem>
                <MenuItem dir="rtl" style={{ backgroundColor: 'white', fontFamily: 'Calibri-light' }} disabled={true} >
                    <ListItemText className="hebrew-text" inset primary="owlhub" />
                    <ListItemIcon > <ChatRounded />
                    </ListItemIcon>
                </MenuItem>
                <Divider></Divider>

                <MenuItem dir="rtl">
                    <RouterLink to={'/ofek'} onClick={() => this.props.closeCallback()}>
                        <ListItemText className="hebrew-text" inset primary="יחידת אופק" /></RouterLink></MenuItem>

                <Divider></Divider>

                {/* Unit List Section */}
                {/* {this.state.isExpanded.length > 0 ? units.map(unit => { */}
                    {units && this.state.isExpanded.length > 0 ? units.map(unit => {
                    return (
                        <React.Fragment>
                            <MenuItem dir="rtl" style={{ backgroundColor: 'white', fontFamily: 'Calibri-light' }} >
                                <RouterLink to={'/ofek/' + unit.unit_id} onClick={() => this.props.closeCallback()}>
                                    <ListItemText className="hebrew-text" dir="rtl" inset primary={unit.unit_name} />
                                </RouterLink>
                                <ListItemIcon onClick={() => this.handleUnitExpandClick(unit)}>
                                    {this.state.isExpanded[unit.unit_id - 1].isUnitExpandad ? <KeyboardArrowDown className="arrow" /> : <KeyboardArrowLeft className="arrow" />}
                                </ListItemIcon>
                            </MenuItem>

                            <Divider />

                            {unit.gafs.map(gaf => {
                                return (
                                    <React.Fragment>
                                        <Collapse in={this.state.isExpanded[unit.unit_id - 1].isUnitExpandad}>
                                            <RouterLink to={'/ofek/' + unit.unit_id + '/' + gaf.gaf_id} onClick={() => this.changeGaf(unit, gaf)}>
                                                <MenuItem dir="rtl" style={{ backgroundColor: '#EAEAEA', fontFamily: 'Calibri-light' }}>
                                                    <ListItemText className="hebrew-text" dir="rtl" inset primary={gaf.gaf_name} />
                                                </MenuItem>
                                            </RouterLink>
                                        </Collapse>
                                    </React.Fragment>)
                            })}
                        </React.Fragment>
                    )
                }) : ""}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    const { hierarchy, selectGaf} = state || { hierarchy: {}, selectGaf:{} }
    return {
        hierarchy,
        selectGaf,
    }
}
export default connect(mapStateToProps)(ProjectsMenu);



