import React, { Component } from 'react';
import './TestsPieChart.css'
import Grid from '@material-ui/core/Grid';

class TestsPieChart extends Component {
    render() {
        return (
            <Grid item xs={6} className="test">
                <div className="canvasParentDiv" style={{ position: "relative", zIndex: "3", width:"15vw"}}>
                    
                    <div style={{ width: "55%", height: "40%", position: "absolute", top: "35%", left: "22%", marginTop: "-20px", textAlign: "center", zIndex: "-1" }}>
                        
                        <div style={{ lineHeight: "40px" , zIndex: "0"}}>
                            <h1 style={{ fontSize: "2rem", fontWeight: "normal", marginBlockStart: "0", marginBlockEnd: "0" }}> {this.props.test.label} </h1>
                        </div>

                        <div style={{ top: "40%", position: "absolute", width: "100%",  zIndex: "0" }}>
                            <h3 style={{ color: "#9ACB62" }}> {this.props.test.passed} </h3>
                            <h3>&nbsp;|&nbsp;</h3>
                            <h3 style={{ color: "#C25149" }}> {this.props.test.failed} </h3>
                        </div>

                    </div>
                    
                    <canvas id={this.props.test.type} height="300" style={{ zIndex:"5" }}>
                    </canvas>

                </div>
            </Grid>
        );
    }

}
export default TestsPieChart;


