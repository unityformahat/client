import React, { Component } from 'react';
import './CriticalBug.css';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

class CriticalBug extends Component {

    render() {
        return (
            <div id="CriticalBug">
                <Card id="BugPaper">
                    <CardActionArea id="action" onClick={event => window.open()}>
                        <CardContent id="BugContent">
                                <div id="bugRedTitle">
                                    <Typography variant="h5" component="h2" align="right" id="bugRedTitleType">
                                        {this.props.bug.bug_name}
                                    </Typography>
                                </div>
                                <div id="bugDesc">
                                    <Typography variant="h5" component="h2" align="right" id="bugDescType" >
                                        {this.props.bug.details}
                                    </Typography>
                                </div>
                        </CardContent>
                    </CardActionArea>
                </Card>
            </div>
        );
    }
}

export default CriticalBug;
