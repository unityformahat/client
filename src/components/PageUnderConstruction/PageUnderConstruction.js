import React, { Component } from 'react';
import { Paper } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import './PageUnderConstruction.css';

class PageUnderConstruction extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }
    render() {
        return (
            <div className="tabPage">
                <Grid item container xs={11} direction="column" className="widgetsSection">
                    <Paper className="pageConstructionPaper">
                        <div>
                            <h1 className="pageUnderConstructionHeader">דף { this.props.pageName } בבנייה</h1>
                            <img className="buildingImg" src='/assets/building.png' alt="" />
                        </div>
                    </Paper>
                </Grid>
            </div>

        );
    }
}

export default PageUnderConstruction;
