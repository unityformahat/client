import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import pretty from "pretty";

import PageUnderConstruction from "./PageUnderConstruction";

let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("renders with or without info", () => {
  const pageName = "ak";

  act(() => {
    render(<PageUnderConstruction />, container);
  });
  expect(container.textContent).toBe("דף  בבנייה");

  act(() => {
    render(<PageUnderConstruction pageName={pageName} />, container);
  });
  expect(container.textContent).toBe("דף ak בבנייה");
});

it("renders with false info", () => {
  const cardInfo1 = {
    ab: "cd",
  };
  act(() => {
    render(<PageUnderConstruction cardInfo={cardInfo1} />, container);
  });
  expect(container.textContent).toBe("דף  בבנייה");
});

it("should render the page", () => {
  act(() => {
    render(<PageUnderConstruction />, container);
  });
  expect(pretty(container.innerHTML)).toMatchInlineSnapshot(`
    "<div class=\\"tabPage\\">
      <div class=\\"MuiGrid-container-249 MuiGrid-item-250 MuiGrid-direction-xs-column-252 MuiGrid-grid-xs-11-288 widgetsSection\\">
        <div class=\\"MuiPaper-root-346 MuiPaper-elevation2-350 MuiPaper-rounded-347 pageConstructionPaper\\">
          <div>
            <h1 class=\\"pageUnderConstructionHeader\\">דף בבנייה</h1><img class=\\"buildingImg\\" src=\\"/assets/building.png\\" alt=\\"\\">
          </div>
        </div>
      </div>
    </div>"
  `);
});
