import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ClearIcon from '@material-ui/icons/Clear';
import CheckIcon from '@material-ui/icons/Check';
import StarIcon from '@material-ui/icons/Star';

class ReportedTeamHoursTable extends Component {

    constructor(props) {
        super(props)
        
        this.state = {
            open:false

        }
    }

    hoursTeamMemberReport = (team_member) => {
        var status=[]
        for (var i = 0; i < team_member.member_hours.length; i++) {

            if(team_member.member_hours[i]<9){
                status.push(<TableCell style={{textAlign:"right", color:"#FB3636"}}>({team_member.member_hours[i]})<ClearIcon/></TableCell>)}
            else if (team_member.member_hours[i]===9){
                status.push(<TableCell style={{textAlign:"right", color:"#71E771"}}>({team_member.member_hours[i]})<CheckIcon/></TableCell>)}
            else {
                status.push(<TableCell style={{textAlign:"right", color:"#FDBD01"}}>({team_member.member_hours[i]})<StarIcon/></TableCell>)}

            }
             return status
}

    render() {
        console.log(this.props.data)
        return (
            <Paper style={{width:'90%', marginRight:"5%", overflowX:'auto'}}>   
                <Table size="small" style={{}}>
                    <TableHead style={{ backgroundColor: "#D3E0F5" }}>
                        <TableRow>
                            <TableCell className="labelcell">שם חבר הצוות</TableCell>
                            {this.props.pastFiveDays()}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.props.data.teams.find(team => {return team.teamname === this.props.current_team_name}).team_members.map(team_member => (
                            <TableRow>
                                <TableCell className="labelcell">{team_member.name}</TableCell>
                                {this.hoursTeamMemberReport(team_member)}  
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </Paper>
                       
        );
        }
    }
    
    export default ReportedTeamHoursTable;