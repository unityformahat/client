import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import LaunchIcon from '@material-ui/icons/Launch';
import IconButton from '@material-ui/core/IconButton';
import ReportedTeamHoursTable from '../ReportedTeamHoursTable/ReportedTeamHoursTable';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import CloseIcon from '@material-ui/icons/Close';
import './ReportedGafHoursTable.css';

class ReportedGafHoursTable extends Component {

    constructor(props) {
        super(props)


        this.state = {
            curruntDate: new Date(),
            open: false,
            dialogs: [false, false, false],
            data:
            {
                teams: [{
                    teamname: "Devops",
                    team_hours: [27, 27, 51, 45, 36],
                    team_members: [
                        { name: "Guy", member_hours: [9, 0, 15, 9, 4] },
                        { name: "Alon", member_hours: [0, 9, 9, 9, 9] },
                        { name: "Hadas", member_hours: [9, 9, 9, 9, 9] },
                        { name: "Doron", member_hours: [0, 0, 9, 9, 5] },
                        { name: "Itay", member_hours: [9, 9, 9, 9, 9] },]
                },
                {
                    teamname: 'תו"ש',
                    team_hours: [9, 9, 18, 0, 9],
                    team_members: [
                        { name: "Raheli", member_hours: [9, 9, 9, 0, 9] },
                        { name: "Kobi", member_hours: [0, 0, 9, 0, 0] }]
                },
                {
                    teamname: "הנדסת בדיקות",
                    team_hours: [18, 18, 18, 13, 9],
                    team_members: [
                        { name: "Levi", member_hours: [9, 9, 9, 4, 0] },
                        { name: "Jenya", member_hours: [9, 9, 9, 9, 9] }]
                },
                {
                    teamname: "ביצועים",
                    team_hours: [27, 27, 45, 45, 36],
                    team_members: [
                        { name: "Guy", member_hours: [9, 0, 9, 9, 4] },
                        { name: "Alon", member_hours: [0, 9, 9, 9, 9] },
                        { name: "Hadas", member_hours: [9, 9, 9, 9, 9] },
                        { name: "Doron", member_hours: [0, 0, 9, 9, 5] },
                        { name: "Itay", member_hours: [9, 9, 9, 9, 9] },]
                }
                ]
            },

            pastFiveDays: this.pastFiveDays

        }
    }

    //returns the next five days from now without weekends as a TableCell
    pastFiveDays = () => {
        var fivedays = [];
        if (this.props.data.last_work_days.length === 5){
            for (var i = 0; i < this.props.data.last_work_days.length; i++) {
                fivedays.push(<TableCell className="labelcell" key={i}>{this.props.data.last_work_days[i]}</TableCell>)
            }
        }
        else {
            for (var i = 4; i >= 0; i--) {
                fivedays.push(<TableCell className="labelcell" key={i}>{this.indexToDate[(this.state.curruntDate.getDay() - i + 5) % 5]}</TableCell>)
            }
        }
       
       
        return fivedays;
    }


    //generates the ammounts of hours reported by the team and colors them
    hoursTeamReport = (team) => {
        var status = []
        for (var i = team.team_hours.length - 1; i >= 0; i--) {
            //green- reported 100% of hours
            if (team.team_members.length * 9 <= team.team_hours[i]) {
                status.push(
                    <TableCell className="hourcell" key={i}>
                        <div style={{ display: "flex" }}>
                            <div style={{ marginTop: "3%" }}>
                                {team.team_members.length * 9}/
                        </div>
                            <div className="background-radius" style={{ color: "#71E771", backgroundColor: "#DAFFDA" }}>
                                {team.team_hours[i]}
                            </div>
                        </div>
                    </TableCell>)
            }
            //yellow- reported 50%+ of hours
            else if (team.team_members.length * 9 / team.team_hours[i] <= team.team_members.length * 4.5) {
                status.push(
                    <TableCell className="hourcell" key={i}>
                        <div style={{ display: "flex" }}>
                            <div style={{ marginTop: "5%" }}>
                                {team.team_members.length * 9}/
                        </div>
                            <div className="background-radius" style={{ color: "#EBD40C", backgroundColor: "#FDFCE8" }}>
                                {team.team_hours[i]}
                            </div>
                        </div>
                    </TableCell>)
            }
            //red- reported less then 50% of hours
            else {
                status.push(
                    <TableCell className="hourcell" key={i}>
                        <div style={{ display: "flex" }}>
                            <div style={{ marginTop: "3%" }}>
                                {team.team_members.length * 9}/
                        </div>
                            <div className="background-radius" style={{ color: "#FB3636", backgroundColor: "#FFD4D4" }}>
                                {team.team_hours[i]}
                            </div>
                        </div>
                    </TableCell>)
            }
        }
        return status
    }

    // functions for handeling dialog for team description
    handleOpen = (teamIndex) => {
        let dialogsCopy = this.state.dialogs;
        dialogsCopy[teamIndex] = true;
        this.setState({ dialogs: dialogsCopy })
    }

    handleClose = (teamIndex) => {
        let dialogsCopy = this.state.dialogs;
        dialogsCopy[teamIndex] = false;
        this.setState({ dialogs: dialogsCopy })
    }


    // this var converts days from day number to hebrew names to display
    indexToDate = {
        4: "ראשון",
        3: "שני",
        2: "שלישי",
        1: "רביעי",
        0: "חמישי",
    }

    render() {
        return (
            <div className="widgetDiv flexed-div">
                <div>
                    <p className="widgetTitle">דיווחי שעות</p>
                </div>
                <div className="widget-body-div" style={{ margin: "0" }}>
                    <Paper className="black-scroll-bar" id="tablePaper" style={{ direction: "ltr"}}>
                        <Table style={{ direction: "rtl"}}>
                            <TableHead style={{ backgroundColor: "#D3E0F5" }}>
                                <TableRow>
                                    <TableCell className="labelcell">שם הצוות</TableCell>
                                    {this.pastFiveDays()}
                                    <TableCell className="labelcell">פירוט הצוות</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody style={{ overflow: "auto" }}>
                                {this.props.data.teams.length === 0 ?
                                this.state.data.teams.map(team => (
                                    <TableRow key={team.teamname}>
                                        <TableCell className="labelcell">{team.teamname}</TableCell>
                                        {this.hoursTeamReport(team)}

                                        <TableCell className="labelcell"><IconButton onClick={() => this.handleOpen(this.state.data.teams.indexOf(team))}><LaunchIcon /></IconButton></TableCell>
                                        <Dialog dir='rtl' maxWidth='lg' fullWidth={true}
                                            onClose={() => this.handleClose(this.state.data.teams.indexOf(team))} open={this.state.dialogs[this.state.data.teams.indexOf(team)]}>
                                            <DialogTitle className="labelcell">
                                                <IconButton onClick={() => this.handleClose(this.state.data.teams.indexOf(team))} open={this.state.dialogs[this.state.data.teams.indexOf(team)]} style={{ marginLeft:"1rem" }}>
                                                    <CloseIcon />
                                                </IconButton>
                                                דיווח שעות הצוות

                                            </DialogTitle>
                                            <DialogContent >
                                                <ReportedTeamHoursTable data={this.state.data} pastFiveDays={this.state.pastFiveDays} current_team_name={team.teamname} />
                                            </DialogContent>
                                        </Dialog>
                                    </TableRow>
                                ))
                                :
                                this.props.data.teams.map(team => (
                                    <TableRow key={team.teamname}>
                                        <TableCell className="labelcell">{team.teamname}</TableCell>
                                        {this.hoursTeamReport(team)}

                                        <TableCell className="labelcell"><IconButton onClick={() => this.handleOpen(this.props.data.teams.indexOf(team))}><LaunchIcon /></IconButton></TableCell>
                                        <Dialog dir='rtl' maxWidth='lg' fullWidth={true}
                                            onClose={() => this.handleClose(this.props.data.teams.indexOf(team))} open={this.state.dialogs[this.props.data.teams.indexOf(team)]}>
                                            <DialogTitle className="labelcell">
                                                <IconButton onClick={() => this.handleClose(this.props.data.teams.indexOf(team))} open={this.state.dialogs[this.props.data.teams.indexOf(team)]} style={{ marginLeft:"1rem" }}>
                                                    <CloseIcon />
                                                </IconButton>
                                                דיווח שעות הצוות

                                            </DialogTitle>
                                            <DialogContent >
                                                <ReportedTeamHoursTable data={this.props.data} pastFiveDays={this.pastFiveDays} current_team_name={team.teamname} />
                                            </DialogContent>
                                        </Dialog>
                                    </TableRow>
                                ))
                                }
                            </TableBody>
                        </Table>
                    </Paper>
                </div>
            </div>
        );
    }
}

export default ReportedGafHoursTable;
