import React, { Component } from 'react';
import './BreadCrumbs.css';
import { withRouter } from 'react-router-dom';
import { Chip } from '@material-ui/core';
import Swal from 'sweetalert2';
import { connect } from 'react-redux';

class BreadCrumbs extends Component {
  constructor(props) {
    super(props)
    this.state = {
      chipData: [
        { key: 0, label: "פרויקט א" },
        { key: 1, label: "פרויקט ב" },
        { key: 2, label: "פרויקט ג" },
      ],
    }
  }

  handleClick(clickedChipData) {
    Swal.fire({
      icon: 'info',
      title: "לחצת על פרוייקט",
      text: clickedChipData.label,
      confirmButtonText: 'קול'
    })
  }


  render() {
    const { selectedGaf, selectedUnit} = this.props;

    return (
      <div className="BreadCrumbs">
        {selectedUnit && selectedGaf ?
        <span id="tree">
          {/* <a href={'/'}>  אופק 324 </a> / */}
          <a href={'/'}>  ינשופים </a> /
              <a href={'/ofek/' + selectedUnit.unit_id}>{selectedUnit.unit_name}</a>
          {selectedGaf && <a href={'/ofek/' + selectedUnit.unit_id + '/' + selectedGaf.gaf_id}>/{selectedGaf.gaf_name}</a>}
        </span>
        : ""}
        <div id="filterByProject">
          {this.state.chipData.map(data => {
            let icon;

            if (data.label === 'React') {
              icon = "";
            }

            return (
              <Chip
                key={data.key}
                icon={icon}
                label={data.label}
                className="chip"
                onClick={(e) => this.handleClick(data)}
              />
            );
          })}
          :סינון לפי פרוייקט
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { selectedGaf, selectedUnit} = state.selectGaf || { selectedGaf: {}, selectedUnit:{} }
  return {
    selectedGaf,
    selectedUnit,
  }
}
export default connect(mapStateToProps, withRouter)(BreadCrumbs);


