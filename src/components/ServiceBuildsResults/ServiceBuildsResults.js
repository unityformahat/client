import React, { Component } from 'react';
import './ServiceBuildsResults.css';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import Divider from '@material-ui/core/Divider';
import CheckIcon from '@material-ui/icons/Check';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';

class ServiceBuildsResults extends Component {

    failedColor = "C25149"
    successColor = "9ACB62"

    getClassName() {
        if (this.props.result === "failed") {
            return "failedClass"
        } else {
            return "successClass"
        }
    }

    render() {
        return (
            <div id="ServiceBuildsResults">
                <p className="flexed-title">
                    <div id="numOfServices" className={this.props.result+"-build-text"}>{this.props.builds.length}</div>
                    {this.props.result === "failed" ?
                        <div className={"buildsResultTitle failed-build-text"}>לא עברו</div>
                        : <div className={"buildsResultTitle success-build-text"}>עברו</div>}
                </p>
                <div id="div-over-rect">
                    <div id="rectBuilds" className={"rectBuilds" + this.props.result + " bodyBugsDiv"} >
                        <GridList cellHeight={40} className="serviceGridList black-scroll-bar" spacing={8}>
                            {this.props.builds.map((build) => (
                                <div style={{width: "92%", height:"max-content"}} key={build.key}>
                                    <GridListTile className="oneBuildResult" cols={2} key={build.key}>
                                        <div id="serviceLastBuild">
                                            {this.props.result === "failed" ?
                                            <ErrorOutlineIcon className="icon-margin-size failed-build-text"></ErrorOutlineIcon> :
                                            <CheckIcon className="icon-margin-size success-build-text"></CheckIcon>}
                                            <div style={{ fontSize: "calc(0.3vw + 14px)", paddingTop: "1%", color: "white", fontWeight: "bold"}}>{build.serviceName}</div>
                                        </div>
                                    </GridListTile>
                                    <Divider />
                                </div>
                            ))}
                        </GridList>
                    </div>
                </div>
            </div>
        );
    }
}

export default ServiceBuildsResults;
