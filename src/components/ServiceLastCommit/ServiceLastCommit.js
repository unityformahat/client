import React, { Component } from 'react';
import './ServiceLastCommit.css';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import MessageIcon from '@material-ui/icons/Message';


class ServiceLastCommit extends Component {
    render() {
        return (
            <React.Fragment>
                {/* Service Last Commit */}
                {/* A better implementation would've created a service card header component! */}
                <Grid item xs={3} sm={3} style={{ maxHeight:"100%"}}>
                    <Card className="servicePaper">
                        <CardActionArea id="action" onClick={event => window.open("https://bamboo.app.iaf/browse/" + this.props.serviceInfo.bambooPlanKey)}>
                            <CardContent id="content">
                                <Grid container spacing={16} xs={16} direction="row" className="height">
                                    <Grid item container xs={15} sm direction="column" className="height">
                                        <Grid item container xs={3} className="height" style={{ maxWidth: "100%", justifyContent: "space-between" }}>
                                            <Grid item xs={3} id="dateGrid" className="height" style={{ maxWidth: "100%" }}>
                                                <div id="lastCommitdate">{new Date(this.props.commitsInfo.pull_request_date).toLocaleDateString('en-GB')}</div>
                                            </Grid>
                                        </Grid>
                                        <Divider variant="middle" style={{ backgroundColor: "#6A7AA2" }} />

                                        <Grid item container direction="row" xs={8} id="lastCommitContent" style={{maxWidth: "100%", overflowY:"auto", maxHeight:"100%"}}>
                                            <Grid item xs={3} id="messageIconContainer" >
                                                    <MessageIcon id="lastCommitMessageIcon"></MessageIcon>
                                            </Grid>
                                            <Grid item xs={9} id="commitMessageContainer" className="black-scroll-bar" style={{overflowY:"auto", maxHeight:"100%"}}>
                                            {this.props.commitsInfo.commits.map((commit) =>
                                                    <div className="lastCommitMessage"> {'"' + commit + '"'}</div>
                                            )
                                            }
                                            </Grid>




                                        </Grid>
                                    </Grid>
                                    <Grid item xs={1} id="buildResult" style={{ backgroundColor: "#6A7AA2" }}>
                                    </Grid >

                                </Grid>
                            </CardContent>
                        </CardActionArea>
                    </Card>
                </Grid>
            </React.Fragment>
        );
    }
}

export default ServiceLastCommit;
