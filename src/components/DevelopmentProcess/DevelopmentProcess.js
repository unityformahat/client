import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepButton from '@material-ui/core/StepButton';
import StepLabel from '@material-ui/core/StepLabel';
import DevelopmentStepIcon from '../DevelopmentStepIcon/DevelopmentStepIcon.js';
import './DevelopmentProcess.css';
import { StepConnector } from '@material-ui/core';


function getSteps() {
    return ['סיכום', 'ניהול', 'פיתוח', 'בדיקות', 'ניטור', 'מדדים אוטמטיים'];
}

class DevelopmentProcess extends Component {
    constructor(props) {
        super(props);
        this.state = {
            scrollPos: 0,
            activeStep: 0,
            completed: new Set(),
            skipped: new Set(),
        };
        this.scrolling = this.scrolling.bind(this);
    }

    totalSteps = () => {
        return getSteps().length;
    };

    isStepComplete(step) {
        return this.state.completed.has(step);
    }

    stepClickFunc(index) {
        if (!this.props.isLoading) {
            var scrollingOffset = 115;
            if (window.screen.width > 2000) {
                scrollingOffset = 175;
            }

            switch (index) {
                case 0:
                    var summary = document.getElementById("summary");
                    window.scrollTo(0, summary.offsetTop - scrollingOffset);
                    break;
                case 1:
                    var management = document.getElementById("management");
                    window.scrollTo(0, management.offsetTop - scrollingOffset);
                    break;
                case 2:
                    var development = document.getElementById("development");
                    window.scrollTo(0, development.offsetTop - scrollingOffset);
                    break;
                case 3:
                    var qa = document.getElementById("qa");
                    window.scrollTo(0, qa.offsetTop - scrollingOffset);
                    break;
                case 4:
                    var monitor = document.getElementById("monitor");
                    window.scrollTo(0, monitor.offsetTop - scrollingOffset);
                    break;
                case 5:
                    var automatic = document.getElementById("automatic");
                    window.scrollTo(0, automatic.offsetTop - scrollingOffset);
                    break;
                default: break;
            }
        }
    }

    componentDidMount() {
        window.addEventListener('scroll', this.scrolling);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.scrolling);
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.isLoading) {
            this.setState({
                activeStep: 0,
            })
        }
    }
    resetActiveStep() {
        this.setState({
            activeStep: 0,
        })
    }

    scrolling = () => {
        if (!this.props.isLoading) {
            var sectionOffset = 130;
            if (window.screen.width > 2000) {
                sectionOffset = 175;
            }
            var summary = document.getElementById("summary");
            var summaryOffset = summary.offsetTop;

            var management = document.getElementById("management");
            var managementOffset = management.offsetTop;

            var development = document.getElementById("development");
            var developmentOffset = development.offsetTop;

            var qa = document.getElementById("qa");
            var qaOffset = qa.offsetTop;

            var monitor = document.getElementById("monitor");
            var monitorOffset = monitor.offsetTop;

            var automatic = document.getElementById("automatic");
            var automaticOffset = automatic.offsetTop;

            if ((window.pageYOffset >= (summaryOffset - sectionOffset)) && (window.pageYOffset < (managementOffset - sectionOffset))) {
                this.setState({
                    activeStep: 0
                })
            }
            else if ((window.pageYOffset >= (managementOffset - sectionOffset)) && (window.pageYOffset < (developmentOffset - sectionOffset))) {
                this.setState({
                    activeStep: 1
                })
            }
            else if ((window.pageYOffset >= (developmentOffset - sectionOffset)) && (window.pageYOffset < (qaOffset - sectionOffset))) {
                this.setState({
                    activeStep: 2
                })
            }
            else if ((window.pageYOffset >= (qaOffset - sectionOffset)) && (window.pageYOffset < (monitorOffset - sectionOffset))) {
                this.setState({
                    activeStep: 3
                })
            }
            else if ((window.pageYOffset >= (monitorOffset - sectionOffset)) && (window.pageYOffset < (automaticOffset - sectionOffset))) {
                this.setState({
                    activeStep: 4
                })
            }
            else if (window.pageYOffset >= (automaticOffset - sectionOffset)) {
                this.setState({
                    activeStep: 5
                })
            }
        }
    }


    render() {
        const steps = getSteps();
        const { activeStep } = this.state;

        return (
            <div id="processStepperRoot">
                <Stepper id="processStepper" alternativeLabel nonLinear activeStep={activeStep} connector={""}>
                    {steps.map((label, index) => {
                        const props = {
                        };
                        const buttonProps = {};
                        if (index === 0) {
                            return (
                                <Step style={{ height: "100%", width: "100%" }} key={label} {...props}>
                                    <StepConnector className="stepConnector"></StepConnector>
                                    <StepButton style={{ height: "100%", padding: "0", margin: "0", width: "100%" }} onClick={(e) => this.stepClickFunc(index)}>
                                        <StepLabel classes={{ label: "labelStyle", iconContainer: "CircleIcon" }} StepIconComponent={DevelopmentStepIcon}
    
                                            completed={this.isStepComplete(index)}
                                            {...buttonProps}>
                                            {label}
                                        </StepLabel>
                                    </StepButton>
                                </Step>
                            );
                            
                        }
                        if (index === steps.length -1) {
                            return (
                                <Step style={{ height: "100%", width: "100%" }} key={label} {...props}>
                                    <StepButton style={{ height: "100%", padding: "0", margin: "0", width: "100%" }} onClick={(e) => this.stepClickFunc(index)}>
                                        <StepLabel classes={{ label: "labelStyle", iconContainer: "CircleIcon" }} StepIconComponent={DevelopmentStepIcon}
    
                                            completed={this.isStepComplete(index)}
                                            {...buttonProps}>
                                            {label}
                                        </StepLabel>
                                    </StepButton>
                                </Step>
                            );
                        }
                        return (
                            <Step style={{ height: "100%", width: "100%" }} key={label} {...props}>
                                <StepConnector className="stepConnector"></StepConnector>
                                <StepButton style={{ height: "100%", padding: "0", margin: "0", width: "100%" }} onClick={(e) => this.stepClickFunc(index)}>
                                    <StepLabel classes={{ label: "labelStyle", iconContainer: "CircleIcon" }} StepIconComponent={DevelopmentStepIcon}

                                        completed={this.isStepComplete(index)}
                                        {...buttonProps}>
                                        {label}
                                    </StepLabel>
                                </StepButton>
                            </Step>
                        );
                    }
                    )}
                </Stepper>

            </div>
        );
    }
}

DevelopmentProcess.propTypes = {
    classes: PropTypes.object,
};
export default DevelopmentProcess;