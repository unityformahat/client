import React, { Component } from 'react';
import BugReportOutlinedIcon from '@material-ui/icons/BugReportOutlined';
import AssignmentOutlinedIcon from '@material-ui/icons/AssignmentOutlined';
import PeopleOutline from '@material-ui/icons/PeopleOutline';
import DesktopWindowsOutlinedIcon from '@material-ui/icons/DesktopWindowsOutlined';
import RemoveRedEyeOutlinedIcon from '@material-ui/icons/RemoveRedEyeOutlined';
import SettingsOutlinedIcon from '@material-ui/icons/SettingsOutlined';
import classNames from "classnames"
import './DevelopmentStepIcon.css'

const icons = {
    1: <AssignmentOutlinedIcon></AssignmentOutlinedIcon>,
    2: <PeopleOutline></PeopleOutline>,
    3: <DesktopWindowsOutlinedIcon></DesktopWindowsOutlinedIcon>,
    4: <BugReportOutlinedIcon></BugReportOutlinedIcon>,
    5: <RemoveRedEyeOutlinedIcon></RemoveRedEyeOutlinedIcon>,
    6: <SettingsOutlinedIcon></SettingsOutlinedIcon>
}

class DevelopmentStepIcon extends Component {

    render() {
        // set conditional class names
        var iconClasses= classNames(
            'defaultIcon',
            {
                'active': this.props.active,
                'completed': this.props.completed
            }
        )

        return (
            <div className={iconClasses}>
                {
                    icons[String(this.props.icon)]
                }
            </div>
            );
    }

}
export default DevelopmentStepIcon;


