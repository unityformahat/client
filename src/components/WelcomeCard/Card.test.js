import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import pretty from "pretty";

import Card from "./Card";

let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("renders with or without info", () => {
  const cardInfo1 = {};
  const cardInfo2 = {
    imagePath: "/assets/logo4.JPG",
  };
  const cardInfo3 = {
    header: "abcd",
    content: "efgh",
  };
  act(() => {
    render(<Card />, container);
  });
  expect(container.textContent).toBe("");

  act(() => {
    render(<Card cardInfo={cardInfo1} />, container);
  });
  expect(container.textContent).toBe("");

  act(() => {
    render(<Card cardInfo={cardInfo2} />, container);
  });
  expect(container.textContent).toBe("");

  act(() => {
    render(<Card cardInfo={cardInfo3} />, container);
  });
  expect(container.textContent).toBe("abcdefgh");
});

it("renders with false info", () => {
  const cardInfo1 = {
    ab: "cd",
  };
  const cardInfo2 = {
    imagePath: "/assets/logo4.JPG",
    ab: "cd",
  };
  const cardInfo3 = {
    header: "abcd",
    content: "efgh",
    ab: "cd",
  };

  act(() => {
    render(<Card cardInfo={cardInfo1} />, container);
  });
  expect(container.textContent).toBe("");

  act(() => {
    render(<Card cardInfo={cardInfo2} />, container);
  });
  expect(container.textContent).toBe("");

  act(() => {
    render(<Card cardInfo={cardInfo3} />, container);
  });
  expect(container.textContent).toBe("abcdefgh");
});

it("should render a card", () => {
  const cardInfo = {
    imagePath: "/assets/logo4.JPG",
    header: "תהנו מהדרך",
    content:
      "התמידו וצברו נקודות, התחרו עם שאר הצוותים ביחידה והכניסו כיף ליום העבודה",
  };
  act(() => {
    render(<Card cardInfo={cardInfo} />, container);
  });
  expect(pretty(container.innerHTML)).toMatchInlineSnapshot(`
    "<div class=\\"Card\\">
      <div class=\\"img\\"><img class=\\"logo\\" src=\\"/assets/logo4.JPG\\" alt=\\"\\"></div>
      <header>
        <h1 class=\\"card-head\\">תהנו מהדרך</h1>
      </header>
      <div>
        <p class=\\"card-body\\">התמידו וצברו נקודות, התחרו עם שאר הצוותים ביחידה והכניסו כיף ליום העבודה</p>
      </div>
    </div>"
  `);
});
