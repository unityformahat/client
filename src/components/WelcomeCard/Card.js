import React, { Component } from 'react';
//import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import './Card.css';

class Card extends Component {
  render() {
    return (
        <div className="Card">
            <div className="img">
              <img className="logo" src={ undefined !== this.props.cardInfo && undefined !== this.props.cardInfo.imagePath ? this.props.cardInfo.imagePath : ""} alt=""/>
            </div>
            <header>
              <h1 className="card-head">{undefined !== this.props.cardInfo && undefined !== this.props.cardInfo.header ? this.props.cardInfo.header : ""}</h1>
            </header>
            <div>
              <p className="card-body">{undefined !==this.props.cardInfo && undefined !== this.props.cardInfo.content ? this.props.cardInfo.content : ""}</p>
            </div>
        </div>
    );
  }
}

export default Card;


