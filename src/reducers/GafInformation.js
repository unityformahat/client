import {
    REQUEST_GAF_INFORMATION,
    RECIEVE_GAF_INFORMATION
} from '../actions/index.js'

// a reducer that describes how to change the selected gaf in the store

function gafInformation(state = {}, action) {
    console.log(action);
    switch(action.type) {
        case (REQUEST_GAF_INFORMATION):
            return Object.assign({}, state, {
                isFetching: true,
                didInvalidate: false
            })
        case RECIEVE_GAF_INFORMATION:
            return Object.assign({}, state, {
                gafId: action.gaf,
                isFetching: false,
                gafData: action.newInfo,
                didInvalidate: false,
            })
        default:
            return state
    }
}

export default gafInformation;