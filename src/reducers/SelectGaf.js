import {
    RECIEVE_GAF,
} from '../actions/index.js'

// a reducer that describes how to change the selected gaf in the store

function recieveGaf(state = '', action) {
    switch(action.type) {
        case RECIEVE_GAF:
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                selectedUnit: action.unit,
                selectedGaf: action.gaf
            })
        default:
            return state
    }
}


export default recieveGaf;