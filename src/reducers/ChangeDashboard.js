const initialState= {
    data: {}
}

const ChangeDashboard = (state = initialState, action) => {
    switch (action.type) {
        case 'unit_dashboard':
            return { ...state, data:action.data}
        case 'flight_dashboard':
            const newState = { 
                ...state,
                data: {
                    buildsInfo: action.data
                }};
            console.log(newState);
            return newState;
        default:
            return state
    }
}

export default ChangeDashboard;