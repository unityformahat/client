import {
    INVALIDATE_HIERARCHY,
    REQUEST_HIERARCHY,
    RECIEVE_HIERARCHY,
    REQUEST_GAF_INFORMATION,
    RECIEVE_GAF_INFORMATION
} from '../actions/index'

function getUnitIndex(unitsArray, unitID) {
    var i=0;
    while (i < Object.keys(unitsArray).length) {
        if (unitsArray[i].unit_id === unitID) {
            return i;
        }
        i+=1;
    }
}

function getGafIndex(gafsArray, gafID) {
    var i=0;
    while (i < Object.keys(gafsArray).length) {
        if (gafsArray[i].gaf_id === gafID) {
            return i;
        }
        i+=1;
    }
}

function hierarchy(
    state = {
        isFetching: false,
        didInvalidate: false,
        units: []
    },
    action
) {
    switch (action.type) {
        case (INVALIDATE_HIERARCHY):
            return Object.assign({}, state, {
                didInvalidate: true
            })
        case (REQUEST_HIERARCHY):
            return Object.assign({}, state, {
                isFetching: true,
                didInvalidate: false
            })
        case (RECIEVE_HIERARCHY):
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                units: action.hierarchy,
                lastUpdated: action.recivedAt
            })
        case (REQUEST_GAF_INFORMATION):
            var newStateReq =JSON.parse(JSON.stringify(state));
            var unitIndex = getUnitIndex(newStateReq.units, action.unit);
            var gafIndex = getGafIndex(newStateReq.units[unitIndex].gafs, action.gaf);
            newStateReq.units[unitIndex].gafs[gafIndex].gafInformation = {
                isFetching: true,
                didInvalidate: false
            };
            return Object.assign({}, state, newStateReq)
        case (RECIEVE_GAF_INFORMATION):
            var newStateRec =JSON.parse(JSON.stringify(state));
            unitIndex = getUnitIndex(newStateRec.units, action.unit);
            gafIndex = getGafIndex(newStateRec.units[unitIndex].gafs, action.gaf);
            newStateRec.units[unitIndex].gafs[gafIndex].gafInformation = {
                gafId: action.gaf,
                isFetching: false,
                didInvalidate: false,
                gafData: action.newInfo,
                lastUpdated: action.recivedAt
            };
            return Object.assign({}, state, newStateRec)
        default:
            return state
    }
}

export default hierarchy;