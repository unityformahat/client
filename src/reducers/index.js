import selectGaf from './SelectGaf';
import hierarchy from './Hierarchy';
// import gafInformation from './GafInformation';
import {combineReducers} from 'redux';

const rootReducer = combineReducers({
    selectGaf,
    hierarchy,
    // gafInformation
})

export default rootReducer;