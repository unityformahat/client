import React from 'react';
import { useEffect } from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Welcome from './pages/WelcomePage/WelcomePage.js';
import GafInfoPage from './pages/GafInfoPage/GafInfoPage.js';
import SideBar from './components/SideBar/SideBar';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core';
import UnitInfo from './pages/UnitInfoPage/UnitInfoPage';
import { fetchHierarchyIfNeeded } from './actions/index.js';
import { connect } from 'react-redux';
import PageLoading from './components/PageLoading/PageLoading';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#44AEFF'
    },
    typography: {
      // Use the system font instead of the default Roboto font.
      fontFamily: "Calibri",
    },

  }
});

function App(props) {
  useEffect(() => {
    getHierarchy();
  },[]);

  const { dispatch } = props;
  const getHierarchy = async () => {
    await dispatch(fetchHierarchyIfNeeded());
  };
  return (
    <div className="App" >
      <Router>
      {
            props.hierarchy.units.length > 0 ?
        <div id="wholeAppPage">
          <div id="nav" dir="rtl" >
            <MuiThemeProvider theme={theme}>
              <SideBar id="appBar" />
            </MuiThemeProvider>
          </div>
          <div className="mainPage">
            <Switch>
              <Route path="/" exact component={Welcome} />
              <Route path="/ofek/:unit/:flight/:project" component={GafInfoPage} />
              <Route path="/ofek/:unit/:flight" component={GafInfoPage} />
              <Route path="/ofek/:unit" component={UnitInfo} />
            </Switch>
          </div>
        </div>
         :  
          <PageLoading></PageLoading>
          }
      </Router>
    </div>
  );
}

const mapStateToProps = (state) => {
  const hierarchy = state.hierarchy;
  return {
    hierarchy
  }
} 
export default connect(mapStateToProps)(App);