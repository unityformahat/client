let state= {
    isFetching: false,
    builds=[{"id" : 1,
             "..." : "..."},
             {"id" : 2,
             "..." : "..."}],
    bugs=[],
    sprints=[],
    workReports=[],
    units: [{
        id: 1,
        name: "maan",
        hebrewName: 'מה"ן',
        gafs: [{
            id: 1,
            name: "TmunaAvirit",
            hebrewName:"תמונה אווירית",
            gafInformation: {
                builds: [1,5,8],
                bugs: [4,9,10],
                sprints: [1,5,8],
                workReports: [4,9,10],
                didInvalidate: false
            }
        },
        {
            id: 1,
            name: "TmunatShamaim",
            hebrewName:"תמונת שמיים",
            gafInformation: {
                builds: [1,5,8],
                bugs: [4,9,10],
                sprints: [1,5,8],
                workReports: [4,9,10],
                didInvalidate: false
            }
        }]
    },{
        id: 2,
        name: "matan",
        hebrewName: 'מת"ן',
        gafs: [{}]
        }
    ]
}
