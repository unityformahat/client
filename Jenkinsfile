pipeline { 
    environment { 
        IMAGE_NAME = "devopsofekshaked/react-client:${BUILD_NUMBER}" 
        DOCKERHUB_USER = "devopsofekshaked" 
        DOCKERHUB_PASS = credentials('dockerhub_pass') 
        NAMESPACE = "unity"
        SONAR_TOKEN = credentials('sonar_token')
        SONAR_HOST_URL= "http://sonarqube.owls.shlomke.xyz/"
    } 
    agent any
    stages {
        stage('Install Dependencies') {
            steps {
                sh 'npm i'
            }
        }
        stage('Test') {
            steps {
                sh 'npm run test-ci'
                stash(name: "coverage_report", includes: 'coverage/lcov.info')
            }
        }
        stage('SonarQube') {
            agent { docker { image 'sonarsource/sonar-scanner-cli:latest' } }
            steps{
                unstash("coverage_report")
                sh 'sed -i "s|SF:/.*/src/|SF:/${WORKSPACE}/src/|g" coverage/lcov.info'
                sh 'sed -i "s|SF:/.*/public/|SF:/${WORKSPACE}/public/|g" coverage/lcov.info'
                sh 'branch_name=$(echo ${GIT_BRANCH} | sed "s@/@-@g") && sonar-scanner -Dsonar.projectKey=Owls.Gluz.react_client:${branch_name} -Dsonar.javascript.lcov.reportPaths=coverage/lcov.info'
            }
        }
        stage('Package') {
            steps {
                sh 'npm run build'
            }
        }
        stage('Build Docker image and Push') { 
            when { branch 'master' } 
            steps {
                sh 'docker login -u ${DOCKERHUB_USER} -p ${DOCKERHUB_PASS}' 
                sh 'docker build -t ${IMAGE_NAME} .' 
                sh 'docker push ${IMAGE_NAME}' 
                sh 'docker rmi ${IMAGE_NAME}' 
            } 
        } 
        stage('Deploy to Kubernetes') { 
            when { branch 'master' } 
            steps { 
               withKubeConfig([credentialsId: 'kube-config']) { 
                   sh "sed -i 's/BUILDNUM/${BUILD_NUMBER}/g' k8s/client-deployment.yaml" 
                   sh "kubectl apply -f k8s/client-deployment.yaml --namespace ${NAMESPACE}" 
                   sh "kubectl apply -f k8s/client-service.yaml --namespace ${NAMESPACE}" 
                   sh "kubectl apply -f k8s/client-ingress.yaml --namespace ${NAMESPACE}" 

                } 
            } 
        }
    }
    post { 
        always { 
            deleteDir() 
        } 
    } 

} 

