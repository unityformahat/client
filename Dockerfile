# Base image is nginx
FROM nginx

EXPOSE 80
# Copy compiled code to nginx server
COPY build /usr/share/nginx/html
# Move pics to /assets
RUN mkdir -p /usr/share/nginx/html/assets
RUN mv /usr/share/nginx/html/favicon.ico /usr/share/nginx/html/assets
COPY src/assets/* /usr/share/nginx/html/assets/
# Copy nginx configuration
COPY ./nginx_configs/default.conf /etc/nginx/conf.d
# cd to tmp
WORKDIR /tmp
# Copy environment variable script
COPY ./nginx_configs/env.sh .
# make environment varibla script executable
RUN chmod +x env.sh
# Edit nginx conf file permission
RUN chmod -R 777 /etc/nginx/conf.d/


#temp here
ENV UNITY_API_URL=http://unity.owls.shlomke.xyz

# Run nginx server
CMD /bin/bash -c "./env.sh && exec nginx -g 'daemon off;'"
